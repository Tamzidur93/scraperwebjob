﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScraperWebAPI.models
{
    public class Site
    {

        public Site()
        {

        }

        public Site(string name, string siteURL, string lastModified)
        {
            Name = name;
            SiteURL = siteURL;
            LastModified = lastModified;
        }

        [Key]
        public int SiteID { get; set; }

        [Index(IsUnique = true), StringLength(450), Required(AllowEmptyStrings = false)]
        public string Name { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string SiteURL { get; set; }

        [Required]
        public string LastModified { get; set; }

        public string LastUpdated { get; set; }

        public ICollection<Source> Sources { get; set; }

        public override bool Equals(object obj)
        {

            var item = obj as Site;


            if (item == null || GetType() != item.GetType())
            {
                return false;
            }

            return Name.ToLower().Trim().Equals(item.Name.ToLower().Trim());
        }

        public override int GetHashCode()
        {
            return 0;
        }
    }
}
