﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScraperWebAPI.models
{
    public class Source
    {
        public Source()
        {

        }

        public Source(string sourceURL, Manga manga, Site site, string lastModified)
        {
            SourceURL = sourceURL;
            Manga = manga;
            Site = site;
            MangaID = manga.MangaID;
            SiteID = site.SiteID;
            LastModified = lastModified;
        }

        [Key]
        public int SourceID { get; set; }

        [Required]
        public string SourceURL { get; set; }

        [Required, ForeignKey("Manga"), Index(name: "IX_SiteMap_Unique", order: 1, IsUnique = true)]
        public int MangaID { get; set; }

        [Required, ForeignKey("Site"), Index(name: "IX_SiteMap_Unique", order: 0, IsUnique = true)]
        public int SiteID { get; set; }

        [Required]
        public virtual Manga Manga { get; set; }
        [Required]
        public virtual Site Site { get; set; }
        [Required]
        public string LastModified { get; set; }

        // nullable
        //public string UpdateFreqSequence { get; set; } = null; // csv

        public Detail Detail { get; set; }
        public ICollection<Chapter> Chapters { get; set; }

        //[Required]
        //public bool Dirty { get; set; } = false;

        //[Required]
        //public bool Running { get; set; } = false;

        // override object.Equals
        public override bool Equals(object obj)
        {
            var item = obj as Source;

            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            return SourceURL.ToLower().Trim().Equals(item.SourceURL.ToLower().Trim())
                && MangaID.Equals(item.MangaID) 
                && SiteID.Equals(item.SiteID);
        }

        // override object.GetHashCode
        public override int GetHashCode()
        {
            return 0;
        }

    }
}
