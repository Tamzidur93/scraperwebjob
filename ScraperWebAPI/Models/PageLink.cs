﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ScraperWebAPI.models
{
    public class PageLink
    {
        public PageLink()
        {

        }

        public PageLink(int pageIndex, string imageURL, string pageURL, Chapter chapter, string lastModified)
        {
            PageIndex = pageIndex;
            ImageURL = imageURL;
            PageURL = pageURL;
            Chapter = chapter;
            ChapterID = chapter.ChapterID;
        }

        [Key, Required]
        public int PageLinkID { get; set; }

        [Required, Index(name: "IX_PageIndexFK_Unique", Order = 1, IsUnique = true)]
        public int PageIndex { get; set; }

        [Required, ForeignKey("Chapter"), Index(name:"IX_PageIndexFK_Unique", Order = 0, IsUnique = true)]
        public int ChapterID { get; set; }

        [Required]
        public Chapter Chapter { get; set; }

        [Required]
        public string ImageURL { get; set; }

        [Required]
        public string PageURL { get; set; }

        public override int GetHashCode()
        {
            return 0;
        }

        public override bool Equals(object obj)
        {
            var item = obj as PageLink;

            if(obj == null || obj.GetType() != GetType())
            {
                return false;
            }

            bool res = true;

            res = res && GetHashCode().Equals(item.GetHashCode());

            res = res && PageIndex.Equals(item.PageIndex);

            res = res && ChapterID.Equals(item.ChapterID);

            res = res && ImageURL.ToLower().Trim().Equals(item.ImageURL.ToLower().Trim());

            res = res && PageURL.ToLower().Trim().Equals(item.PageURL.ToLower().Trim());
            
            return res;
        }
    }
}