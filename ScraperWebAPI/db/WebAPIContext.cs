﻿using Microsoft.EntityFrameworkCore;
using ScraperWebAPI.models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScraperWebAPI.db
{

    public class WebAPIContext : DbContext
    {
        public WebAPIContext(DbContextOptions options) : base(options) { }

        public DbSet<Manga> Mangas { get; set; }
        public DbSet<Site> Sites { get; set; }
        public DbSet<Source> Sources { get; set; }
        public DbSet<Chapter> Chapters { get; set; }
        public DbSet<PageLink> PageLinks { get; set; }
        public DbSet<Detail> Details { get; set; }

        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
            ////Manga - Site mapping via Source
            //modelBuilder.Entity<Source>()
            //    .HasKey(so => new { so.MangaID, so.SiteID});



        //}


        /*System.InvalidOperationException The DbConfiguration type 'MySql.Data.Entity.MySqlEFConfiguration, MySql.Data.Entity.EF6' 
        specified in the application config file could not be loaded. Make sure that the assembly-qualified 
        name is used and that the assembly is available to the running application. See 
        http://go.microsoft.com/fwlink/?LinkId=260883 for more information
        */



    }
}
