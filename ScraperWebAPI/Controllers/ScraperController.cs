﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ScraperWebAPI.db;
using Newtonsoft.Json;

namespace ScraperWebAPI.Controllers
{
    [Route("api/scraper/")]
    public class ScraperController : Controller
    {

        //        An unhandled exception occurred while processing the request.
        //InvalidOperationException: A suitable constructor for type 'ScraperWebAPI.Controllers.ScraperController' could not be located.Ensure the type is concrete and services are registered for all parameters of a public constructor.
        //Microsoft.Extensions.Internal.ActivatorUtilities.FindApplicableConstructor(Type instanceType, Type[] argumentTypes, out ConstructorInfo matchingConstructor, out Nullable`1[] parameterMap)

        private WebAPIContext _context = null;
        private JsonSerializerSettings _jsonSettings = new JsonSerializerSettings();

        public ScraperController(WebAPIContext context)
        {
            _context = context;

            _jsonSettings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            };
        }

        [HttpGet("mangas")]
        public string Mangas()
        {
            var output = "";
            var dbMangas = _context.Mangas.ToList();

            foreach (var dbManga in dbMangas)
            {
                output += $"{dbManga.MangaID}: " + dbManga.Name + "\n";
            }

            return output;
        }

        [HttpGet("sites")]
        public JsonResult SitesJson()
        {
            var dbSites = _context.Sites.ToList();
            return Json(dbSites, _jsonSettings);
        }

        // sources
        [HttpGet("sourceByID/{sourceID}")]
        public JsonResult SourceJSONBySourceID(int sourceID)
        {
            var dbSource = _context.Sources.Where(so => so.SourceID == sourceID).First();
            return Json(dbSource, _jsonSettings);
        }

        [HttpGet("sourcesBySiteID/{siteID}")]
        public JsonResult SourcesJSONBySiteID(int siteID)
        {
            var dbSources = _context.Sources.Where(so => so.SiteID == siteID).ToList();
            return Json(dbSources, _jsonSettings);
        }

        // details
        [HttpGet("detailByID/{detailID}")]
        public JsonResult DetailJSONByDetailID(int detailID)
        {
            var dbDetail = _context.Details.Where(d => d.DetailID == detailID).First();
            return Json(dbDetail, _jsonSettings);
        }

        [HttpGet("detailBySourceID/{sourceID}")]
        public JsonResult DetailJSONBySourceID(int sourceID)
        {
            var dbDetail = _context.Details.Where(d => d.Source.SourceID == sourceID).First();
            return Json(dbDetail, _jsonSettings);
        }

        [HttpGet("detailsBySiteID/{siteID}")]
        public JsonResult DetailJSONBySiteID(int siteID)
        {
            var dbDetails = _context.Details.Where(d => d.Source.SiteID == siteID).ToList();
            return Json(dbDetails, _jsonSettings);
        }

        // chapters
        [HttpGet("chaptersByID/{chapterID}")]
        public JsonResult ChapterJSONByChapterID(int chapterID)
        {
            var dbChapter = _context.Chapters.Where(ch => ch.ChapterID == chapterID).First();
            return Json(dbChapter, _jsonSettings);
        }

        [HttpGet("chaptersBySourceID/{sourceID}")]
        public JsonResult ChaptersJSONBySourceID(int sourceID)
        {
            var dbChapters = _context.Chapters.Where(ch => ch.SourceID == sourceID).ToList();
            return Json(dbChapters, _jsonSettings);
        }

        [HttpGet("chaptersBySiteID/{siteID}")]
        public JsonResult ChaptersJSONBySiteID(int siteID)
        {
            var dbChapters = _context.Chapters.Where(ch => ch.Source.SiteID == siteID).ToList();
            return Json(dbChapters, _jsonSettings);
        }

        // pagelinks
        [HttpGet("pageLinksBySiteID/{siteID}")]
        public JsonResult PageLinksJSONBySiteID(int siteID)
        {
            var dbPageLinks = _context.PageLinks.Where(p => p.Chapter.Source.SiteID == siteID).ToList();
            return Json(dbPageLinks, _jsonSettings);
        }

        [HttpGet("pageLinksByChapterID/{chapterID}")]
        public JsonResult PageLinksJSONByChapterID(int chapterID)
        {
            var dbPageLinks = _context.PageLinks.Where(p => p.ChapterID == chapterID).ToList();
            return Json(dbPageLinks, _jsonSettings);
        }

        [HttpGet("pageLinksBySourceID/{sourceID}")]
        public JsonResult PageLinksJSONBySourceID(int sourceID)
        {
            var dbPageLinks = _context.PageLinks.Where(p => p.Chapter.SourceID == sourceID).ToList();
            return Json(dbPageLinks, _jsonSettings);
        }
    }
}