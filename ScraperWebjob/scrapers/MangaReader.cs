﻿using HtmlAgilityPack;
using Microsoft.WindowsAzure.Storage.Queue;
using Newtonsoft.Json;
using ScraperWebjob.db;
using ScraperWebjob.helpers;
using ScraperWebjob.models;
using ScraperWebjob.scrapers.tasks;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using static ScraperWebjob.scrapers.tasks.ScraperTask;

namespace ScraperWebjob.scrapers
{
    class MangaReader : ISite
    {
        private bool isCancelled = false;
        private Random r = new Random();
        private const string BASE_URL = "http://www.mangareader.net";
        private const string MANGA_LIST = "/alphabetical";
        private const string SITE_NAME = "MangaReader"; // need to switch to enums

#if DEBUG
        private const int THREAD_STRIDE = 2000;
#else
        private const int THREAD_STRIDE = 500;
#endif

        private Detail ParseDetail(Source source, HtmlDocument doc, string utcTimeNow)
        {
            var detailElements = doc.DocumentNode.SelectNodes("//div[@id='mangaproperties']/table//tr//td"); // (key - value) map format -> stride = 2

            if (detailElements.Count % 2 != 0)
            {
                throw new Exception("MangaReader: detail template has changed");
            }

            Detail detail;

            string alternateName = null;
            string yearOfRelease = null;
            string status = null;
            string author = null;
            string artist = null;
            string genre = null;

            for (int j = 0; j < detailElements.Count(); j += 2)
            {
                var element = detailElements[j];

                var key = element.InnerText.Trim();

                switch (key)
                {
                    case "Alternate Name:":
                        alternateName = detailElements[j + 1].InnerText.Trim();
                        break;
                    case "Year of Release:":
                        yearOfRelease = detailElements[j + 1].InnerText.Trim();
                        break;
                    case "Status:":
                        status = detailElements[j + 1].InnerText.Trim();
                        break;
                    case "Author:":
                        author = detailElements[j + 1].InnerText.Trim();
                        break;
                    case "Artist:":
                        artist = detailElements[j + 1].InnerText.Trim();
                        break;
                    case "Genre:":
                        genre = detailElements[j + 1].InnerText.Trim().Replace(" ", ",");
                        break;
                }
            }

            var mangaSum = doc.DocumentNode.SelectSingleNode("//div[@id='readmangasum']/p").InnerText.Trim();
            var mangaImgURL = doc.DocumentNode.SelectSingleNode("//*[@id='mangaimg']/img").Attributes["src"].Value.Trim();

            detail = new Detail(
                alternateNames: alternateName,
                yearOfRelease: yearOfRelease,
                status: status,
                authors: author,
                artists: artist,
                imageURL: mangaImgURL,
                summary: mangaSum,
                genre: genre,
                lastModified: utcTimeNow,
                source: source
                );

            return detail;
        }

        private List<Chapter> ParseChapters(Source source, HtmlDocument doc, string utcTimeNow)
        {
            var createList = new List<Chapter>();

            var chapterElements = doc.DocumentNode.SelectNodes("//table[@id='listing']//tr[not(contains(@class, 'table_head'))]//a");

            for (int j = 0; j < chapterElements.Count; j++)
            {
                var element = chapterElements[j];
                var chapterName = element.InnerText.Trim();
                var chapterURL = element.Attributes["href"].Value.Trim();
                int index = j + 1;

                var chapter = new Chapter(chapterIndex: index, chapterName: chapterName, chapterURL: chapterURL, source: source, lastModified: utcTimeNow);
                chapter.Dirty = false;
                createList.Add(chapter);
            }
            return createList;
        }

        private List<PageLink> ParseImageLinks(Dictionary<int, string> pageUrls, Chapter chapter)
        {

            var web = new HtmlWeb();
            var createList = new List<PageLink>();
            var utcTimeNow = DateTime.Now.ToUniversalTime().ToString(Helpers.DatabaseTimeFormat);

            // download
            foreach (var index in pageUrls.Keys)
            {
                // return nothing so state gets picked up next iteration
                if (isCancelled)
                {
                    return new List<PageLink>();
                }
                string pageUrl;
                pageUrls.TryGetValue(index, out pageUrl);

                if (pageUrl != null)
                {
                    var doc = web.Load(BASE_URL + pageUrl);
                    System.Threading.Thread.Sleep(1000 * r.Next(1, 5));

                    if (web.StatusCode == HttpStatusCode.OK)
                    {
                        var imgUrl = doc.DocumentNode.SelectSingleNode("//*[@id='img']").Attributes["src"].Value.Trim();

                        if (imgUrl != null)
                        {
                            var pageLink = new PageLink(index, imgUrl, pageUrl, chapter, utcTimeNow);
                            createList.Add(pageLink);
                        }
                    }
                }
            }
            return createList;
        }

        private Site GetSite(WebjobContext context)
        {
            ApplicationState.AcquireSiteTBLock();

            var exists = context.Sites.Where(s => s.Name == SITE_NAME).Count() > 0;

            if (!exists)
            {

                var utcTimeNow = DateTime.Now.ToUniversalTime().ToString(Helpers.DatabaseTimeFormat);

                context.Sites.Add(new Site(name: SITE_NAME, siteURL: BASE_URL, lastModified: utcTimeNow));
                context.SaveChanges();
            }

            ApplicationState.ReleaseSiteTBLock();

            return context.Sites.Where(s => s.Name == SITE_NAME).First();
        }

        public void Initialize(WebjobContext context, ScraperTask scraperTask)
        {
            // download mangas and sources
            var site = GetSite(context);
            var web = new HtmlWeb();
            var doc = web.Load(BASE_URL + MANGA_LIST);
            var outputString = "";
            System.Threading.Thread.Sleep(1000 * r.Next(1, 5));

            if (web.StatusCode != HttpStatusCode.OK)
            {
                Console.WriteLine($">>> [{DateTime.Now.ToString(Helpers.LogTimeFormat)}] MangaReader initialize - uanble to download site catalog - error=" + web.StatusCode);
                return;
            }

            var utcTimeNow = DateTime.UtcNow.ToString(Helpers.DatabaseTimeFormat);

            var listItems = doc.DocumentNode.SelectNodes("//div[@class='series_col']//li");

            var createDictionary = new Dictionary<Manga, Source>();

            var createMangaList = new List<Manga>();
            var updateMangaList = new List<Manga>();
            var deleteMangaList = new List<Manga>();

            var createSourceList = new List<Source>();
            var updateSourceList = new List<Source>();
            var deleteSourceList = new List<Source>();

            foreach (var val in listItems)
            {
                string mangaName = val.FirstChild.InnerHtml;
                string mangaUrl = val.FirstChild.Attributes["href"].Value.Trim();

                var manga = new Manga(name: mangaName, lastModified: utcTimeNow);
                var source = new Source(sourceURL: mangaUrl, manga: manga, site: site, lastModified: utcTimeNow);

                try
                {
                    createDictionary.Add(manga, source);
                }
                catch (ArgumentException)
                {
                    // do nothing
                }
            }

            Console.WriteLine($">>> [{DateTime.Now.ToString(Helpers.LogTimeFormat)}] MangaReader: initialize - Parsed sources: {createDictionary.Count}");

            ApplicationState.AcquireMangaTBLock();
            ApplicationState.AcquireSiteLock(this);

            createMangaList.AddRange(createDictionary.Keys);
            createSourceList.AddRange(createDictionary.Values);

            // mangas
            var dbMangas = context.Mangas.ToList();

            foreach (var dbManga in dbMangas)
            {
                Manga newManga;
                Source newSource;

                if ((newManga = createMangaList.Find(m => m.Equals(dbManga))) != null)
                {
                    createMangaList.Remove(newManga);
                }

                // cannot add references without objectid - framework creates new entries for all objects without id
                // find and replace newsource's manga with dbmanga to prevent duplicate creations in mangatb
                if ((newSource = createSourceList.Find(so => so.Manga.Equals(dbManga))) != null)
                {
                    newSource.Manga = dbManga;
                }
            }

            // sources
            var dbSources = context.Sources
                .Where(so => so.SiteID == site.SiteID)
                .Include(so => so.Manga)
                .Include(so => so.Site)
                .ToList();

            foreach (var dbSource in dbSources)
            {
                Source newSource;
                var dbManga = dbSource.Manga;

                // try to find a match for exfisting source in new source list
                if ((newSource = createSourceList.Find(so => so.Manga.Equals(dbManga))) != null)
                {
                    // if existing source is being modified by another thread or is marked for deletion remove from creation list
                    // newSource should never be null
                    if ((!dbSource.Running && !dbSource.Dirty))
                    {
                        if (!dbSource.Equals(newSource))
                        {
                            dbSource.SourceURL = newSource.SourceURL;
                            dbSource.LastModified = newSource.LastModified;

                            updateSourceList.Add(dbSource);
                        }
                    }
                    createSourceList.Remove(newSource);
                }
                // if no match is found mark source for deletion
                else
                {
                    deleteSourceList.Add(dbSource);
                    dbSource.Dirty = true;
                }
            }

            context.Mangas.AddRange(createMangaList);
            context.Sources.AddRange(createSourceList);

            context.SaveChanges();

            var sourceCount = context.Sources.Where(so => so.SiteID == site.SiteID).Count();

            ApplicationState.ReleaseSiteLock(this);
            ApplicationState.ReleaseMangaTBLock();

#if DEBUG
            var threadCount = sourceCount / THREAD_STRIDE;
            if (sourceCount % THREAD_STRIDE != 0) threadCount++;
#else
            var threadCount = sourceCount / THREAD_STRIDE;
            if (sourceCount % THREAD_STRIDE != 0) threadCount++;
#endif

            if (isCancelled)
            {
                outputString = $">>> [{DateTime.Now.ToString(Helpers.LogTimeFormat)}] MangaReader: initalize - total download: 1 page\n";
                outputString += $"Initialize - Added {createMangaList.Count} Mangas\n";
                outputString += $"Initialize - Added {createSourceList.Count} Sources\n";
                outputString += $"Initialize - Updated {updateSourceList.Count} Sources\n";
                outputString += $"Initialize - Marked {deleteSourceList.Count} Sources for deletion\n";

                Console.Write(outputString);
                return;
            }

            Console.WriteLine($">>> [{DateTime.Now.ToString(Helpers.LogTimeFormat)}] MangaReader: initalize - starting {threadCount} download tasks");

            for (int i = 0; i < threadCount; i++)
            {
                ApplicationState.CreateDownloadTask(site: SiteName.MangaReader, parameters: $"{i}:{threadCount}");
            }

            outputString = $">>> [{DateTime.Now.ToString(Helpers.LogTimeFormat)}] MangaReader: initalize - total download: 1 page\n";
            outputString += $"Initialize - Added {createMangaList.Count} Mangas\n";
            outputString += $"Initialize - Added {createSourceList.Count} Sources\n";
            outputString += $"Initialize - Updated {updateSourceList.Count} Sources\n";
            outputString += $"Initialize - Marked {deleteSourceList.Count} Sources for deletion\n";

            Console.Write(outputString);
        }

        public void DownloadDetails(WebjobContext context, ScraperTask scraperTask)
        {
            var threadNumber = -1;
            var threadCount = -1;
            var outputString = "";
            var pageDownloadCount = 0;
            var site = GetSite(context);

            if (scraperTask.Parameters != null)
            {
                var parameters = scraperTask.Parameters.Split(':');
                threadNumber = int.Parse(parameters[0]);
                threadCount = int.Parse(parameters[1]);
            }

            ApplicationState.AcquireSiteLock(this);

            // TODO change startIndex + 1 to startIndex + 500
            var startIndex = threadNumber * THREAD_STRIDE;

#if DEBUG
            var endIndex = Math.Min(context.Sources.Count(), startIndex + 5);
#else
            var endIndex = Math.Min(context.Sources.Count(), startIndex + THREAD_STRIDE);
#endif

            var dbSources = context.Sources
                .Where(so => so.SiteID == site.SiteID)
                .OrderBy(so => so.SourceID)
                .Skip(startIndex)
                .Take(endIndex - startIndex)
                .Include(so => so.Manga)
                .Include(so => so.Site)
                .Include(so => so.Chapters.Select(c => c.PageLinks))
                .ToList();

            Console.WriteLine($">>> [{DateTime.Now.ToString(Helpers.LogTimeFormat)}] MangaReader: download - threadNumber: {threadNumber + 1}, startIndex: {startIndex}, endIndex: {endIndex}");

            for (int i = 0; i < dbSources.Count(); i++)
            {
                var source = dbSources.ElementAt(i);

                if (source.Running || source.Dirty)
                {
                    dbSources.Remove(source);
                    i--;
                }
                else
                {
                    source.Running = true;
                }
            }

            context.SaveChanges();

            ApplicationState.ReleaseSiteLock(this);

            int sourceIndex = 0;

            // download
            foreach (var dbSource in dbSources)
            {
                if (isCancelled)
                {
                    break;
                }

                var updateFound = false;
                var web = new HtmlWeb();
                var doc = web.Load(BASE_URL + dbSource.SourceURL);
                System.Threading.Thread.Sleep(1000 * r.Next(1, 5));

                if (web.StatusCode != HttpStatusCode.OK)
                {
                    Console.WriteLine($">>> [{DateTime.Now.ToString(Helpers.LogTimeFormat)}] MangaReader download - unable to load sourceUrl:{dbSource.SourceURL}, error:{web.StatusCode}");
                    dbSource.Dirty = true;
                    context.SaveChanges();
                    continue;
                }

                var utcTimeNow = DateTime.Now.ToUniversalTime().ToString(Helpers.DatabaseTimeFormat);
                pageDownloadCount++;

                if (doc == null)
                {
                    // TODO need a better check for deleted sites
                    dbSource.Dirty = true;
                    context.SaveChanges();
                    continue;
                }

                // source detail
                var detail = ParseDetail(dbSource, doc, utcTimeNow);

                var dbDetail = context.Details
                    .Where(d => d.DetailID == dbSource.SourceID)
                    .Include(d => d.Source.Manga)
                    .Include(d => d.Source.Site)
                    .FirstOrDefault();

                // check if detail exists in database for sourceID
                // if exists, update to new
                // if doesnt exist, add new

                if (dbDetail != null)
                {
                    dbDetail.AlternateNames = detail.AlternateNames;
                    dbDetail.Artists = detail.Artists;
                    dbDetail.Authors = detail.Authors;
                    dbDetail.Genre = detail.Genre;
                    dbDetail.ImageURL = detail.ImageURL;
                    dbDetail.Source = detail.Source;
                    dbDetail.Status = detail.Status;
                    dbDetail.Summary = detail.Summary;
                    dbDetail.YearOfRelease = detail.YearOfRelease;
                }
                else
                {
                    context.Details.Add(detail);
                }

                // source chapters
                var createChapterList = ParseChapters(dbSource, doc, utcTimeNow);
                var updateChapterList = new List<Chapter>();
                var deleteChapterList = new List<Chapter>();

                var updateAge = 1;

                if (dbSource.UpdateFreqSequence != null)
                {
                    var updateFreqs = dbSource.UpdateFreqSequence.Split(',');
                    updateAge = int.Parse(updateFreqs[updateFreqs.Length - 1]);
                }

                // condition: ( !(source has no chapters) && !(lastUpdate is older than a day) && !(local and online chapterCounts dont match) && !(any chapter has null or 0 pagelinks))
                // have to check if there are 0 or some pagelinks in case chapter download gets canned early for whatever reason
                var lastUpdated = DateTime.SpecifyKind(DateTime.Parse(dbSource.LastModified), DateTimeKind.Utc);
                if (dbSource.Chapters != null && lastUpdated >= DateTime.Now.AddDays(-updateAge) && dbSource.Chapters.Count == createChapterList.Count)
                {
                    var res = false;

                    foreach (var chapter in dbSource.Chapters)
                    {
                        if (chapter.PageLinks == null || chapter.PageLinks.Count == 0 || chapter.Dirty)
                        {
                            res = true;
                            break;
                        }
                    }

                    // fucking nots
                    if (!res)
                    {
                        Console.WriteLine($">>> [{DateTime.Now.ToString(Helpers.LogTimeFormat)}] MangaReader download - skipped:{dbSource.Manga.Name}, lastUpdated:{lastUpdated.ToString()}, local/online:{dbSource.Chapters.Count}/{createChapterList.Count}");
                        dbSource.LastModified = utcTimeNow;
                        dbSource.Running = false;
                        context.SaveChanges();
                        sourceIndex++;
                        continue;
                    }
                }

                var dbChapters = context.Chapters
                    .Where(c => c.SourceID == dbSource.SourceID)
                    .Include(c => c.Source)
                    .Include(c => c.Source.Manga)
                    .Include(c => c.Source.Site)
                    .ToList();

                // filter
                foreach (var dbChapter in dbChapters)
                {
                    var chapter = createChapterList.Find(c => c.ChapterIndex == dbChapter.ChapterIndex);

                    if (chapter != null)
                    {
                        createChapterList.Remove(chapter);

                        if (!chapter.Equals(dbChapter))
                        {
                            dbChapter.ChapterIndex = chapter.ChapterIndex;
                            dbChapter.ChapterName = chapter.ChapterName;
                            dbChapter.ChapterURL = chapter.ChapterURL;
                            dbChapter.LastModified = chapter.LastModified;

                            updateChapterList.Add(dbChapter);
                        }
                    }
                    else
                    {
                        deleteChapterList.Add(dbChapter);
                    }
                }

                if (createChapterList.Count > 0)
                {
                    updateFound = true;
                }

                context.Chapters.AddRange(createChapterList);
                context.Chapters.RemoveRange(deleteChapterList);

                // have to save and load again
                context.SaveChanges();

                outputString = $">>> [{DateTime.Now.ToString(Helpers.LogTimeFormat)}] MangaReader: download - threadNumber: {threadNumber + 1}/{threadCount}, index: {sourceIndex}/{dbSources.Count}\n";
                outputString += $"download - mangaName: {dbSource.Manga.Name}, sourceUrl: {dbSource.SourceURL}\n";
                outputString += $"download - createChapterList count = {createChapterList.Count}\n";
                outputString += $"download - updateChapterList count = {updateChapterList.Count}\n";
                outputString += $"download - deleteChapterList count = {deleteChapterList.Count}\n";

                // getting source content finished
                Console.Write(outputString);

                // get chapters
                dbChapters = context.Chapters
                    .Where(c => c.SourceID == dbSource.SourceID)
                    .Include(c => c.PageLinks)
                    .ToList();

                // chapter pagelinks
                foreach (var dbChapter in dbChapters)
                {
                    if (isCancelled)
                    {
                        break;
                    }

                    var pageDoc = web.Load(BASE_URL + dbChapter.ChapterURL);
                    pageDownloadCount++;
                    System.Threading.Thread.Sleep(1000 * r.Next(1, 5));

                    if (web.StatusCode != HttpStatusCode.OK)
                    {
                        Console.WriteLine($">>> [{DateTime.Now.ToString(Helpers.LogTimeFormat)}] MangaReader download - unable to load chapterURL:{dbChapter.ChapterURL}, error:{web.StatusCode}");
                        dbChapter.Dirty = true;
                        context.SaveChanges();
                        continue;
                    }

                    var options = pageDoc.DocumentNode.SelectNodes("//*[@id='pageMenu']//option");

                    var pageUrls = new Dictionary<int, String>();

                    var createPageList = new List<PageLink>();
                    var updatePageList = new List<PageLink>();
                    var deletePageList = new List<PageLink>();

                    outputString = $">>> [{DateTime.Now.ToString(Helpers.LogTimeFormat)}] MangaReader: download - threadNumber: {threadNumber + 1}/{threadCount}, index: {sourceIndex}/{dbSources.Count}\n";
                    outputString += $"download - mangaName: {dbSource.Manga.Name}, sourceUrl: {dbSource.SourceURL}\n";
                    outputString += $"download - downloading page links for chapter {dbChapter.ChapterIndex}/{dbChapters.Count}\n";
                    Console.Write(outputString);

                    // download pagelinks
                    for (int j = 0; j < options.Count; j++)
                    {
                        var option = options[j];
                        pageUrls.Add(int.Parse(option.NextSibling.InnerText.Trim()), option.Attributes["value"].Value.Trim());
                    }

                    if (dbChapter.PageLinks.Count != options.Count)
                    {
                        createPageList = ParseImageLinks(pageUrls, dbChapter);
                        pageDownloadCount += createPageList.Count;
                    }
                    else
                    {
                        outputString = $">>> [{DateTime.Now.ToString(Helpers.LogTimeFormat)}] MangaReader: Download - threadNumber: {threadNumber + 1}/{threadCount}, index: {sourceIndex}/{dbSources.Count}\n";
                        outputString += $"Download - mangaName: {dbSource.Manga.Name}, sourceUrl: {dbSource.SourceURL}\n";
                        outputString += $"Download - skipped {dbChapter.ChapterIndex}/{dbChapters.Count}, db contains site count of page links\n";

                        dbSource.LastModified = utcTimeNow;
                        context.SaveChanges();

                        Console.Write(outputString);
                        continue;
                    }

                    var dbPageLinks = context.PageLinks
                         .Where(p => p.ChapterID == dbChapter.ChapterID)
                         .Include(p => p.Chapter)
                         .ToList();

                    // filter
                    foreach (var dbPageLink in dbPageLinks)
                    {
                        var pageLink = createPageList.Find(p => p.PageIndex == dbPageLink.PageIndex);

                        if (pageLink != null)
                        {
                            dbPageLink.ImageURL = pageLink.ImageURL;
                            dbPageLink.PageURL = pageLink.PageURL;
                            dbPageLink.PageIndex = pageLink.PageIndex;

                            updatePageList.Add(dbPageLink);

                            createPageList.Remove(pageLink);
                        }
                        else
                        {
                            deletePageList.Add(dbPageLink);
                        }
                    }

                    if (createPageList.Count > 0)
                    {
                        updateFound = true;
                    }

                    context.PageLinks.AddRange(createPageList);
                    context.PageLinks.RemoveRange(deletePageList);

                    dbSource.LastModified = utcTimeNow;
                    context.SaveChanges();

                    outputString = $">>> [{DateTime.Now.ToString(Helpers.LogTimeFormat)}] MangaReader: download - threadNumber: {threadNumber + 1}/{threadCount}, index: {sourceIndex}/{dbSources.Count}\n";
                    outputString += $"download - mangaName: {dbSource.Manga.Name}, sourceUrl: {dbSource.SourceURL}\n";
                    outputString += $"download - chapterIndex: {dbChapter.ChapterIndex}/{dbChapters.Count}\n";
                    outputString += $"download - createPageLinkList count = {createPageList.Count}\n";
                    outputString += $"download - updatePageLinkList count = {updatePageList.Count}\n";
                    outputString += $"download - deletePageLinkList count = {deletePageList.Count}\n";

                    Console.Write(outputString);
                }

                if (dbSource.UpdateFreqSequence == null)
                {
                    dbSource.UpdateFreqSequence = "1";
                }
                else if (updateFound)
                {
                    var sum = 0d;
                    var avg = 0;
                    var values = dbSource.UpdateFreqSequence.Split(',');
                    var next = 1;

                    for (int i = 0; i < values.Length; i++)
                    {
                        sum += int.Parse(values[i]);
                    }

                    // lastModified should be newer
                    var updateFreq = (int)(DateTime.SpecifyKind(DateTime.Parse(dbSource.LastModified), DateTimeKind.Utc) - lastUpdated).TotalDays;
                    avg = (int)Math.Round(sum / values.Length);

                    if (avg > updateFreq)
                    {
                        next = Math.Max(1, updateFreq);
                        next = Math.Min(14, next);
                        dbSource.UpdateFreqSequence = Helpers.AppendUpdateFreqSequence(values, next);
                    }
                    else
                    {
                        // lower bound 1
                        next = Math.Max(1, avg);
                        // max bound 14
                        next = Math.Min(14, next);
                        dbSource.UpdateFreqSequence = Helpers.AppendUpdateFreqSequence(values, next);
                    }
                }
                else
                {
                    var values = dbSource.UpdateFreqSequence.Split(',');
                    // max bound 14
                    var next = Math.Min(int.Parse(values[values.Length - 1]) + 3, 14);

                    dbSource.UpdateFreqSequence = Helpers.AppendUpdateFreqSequence(values, next); ;
                }

                dbSource.Running = false;
                context.SaveChanges();

                sourceIndex++;
            }

            outputString = $">>> [{DateTime.Now.ToString(Helpers.LogTimeFormat)}] MangaReader: download - threadNumber: {threadNumber + 1}/{threadCount}, index: {sourceIndex}/{dbSources.Count}\n";
            outputString += $"Download - total download: {pageDownloadCount} pages\n";

            Console.Write(outputString);

            ApplicationState.AcquireSiteLock(this);

            foreach (var dbSource in dbSources)
            {
                if (dbSource.Running)
                {
                    // if downloaddetails is force cancelled - check unchecked sources in 1 day and adjust avging system
                    if (dbSource.UpdateFreqSequence != null)
                    {
                        var values = dbSource.UpdateFreqSequence.Split(',');
                        dbSource.UpdateFreqSequence = Helpers.AppendUpdateFreqSequence(values, 1);
                    }
                    else
                    {
                        dbSource.UpdateFreqSequence = "1";
                    }

                    dbSource.Running = false;
                }
            }

            context.SaveChanges();

            ApplicationState.ReleaseSiteLock(this);
        }

        public void Update(WebjobContext context, ScraperTask scraperTask)
        {
            var site = GetSite(context);

            ApplicationState.AcquireSiteTBLock();
            ApplicationState.AcquireSiteLock(this);

            var dirtySourceCount = context.Sources.Where(s => s.SiteID == site.SiteID && s.Dirty).Count();
            var runningSourceCount = context.Sources.Where(s => s.SiteID == site.SiteID && s.Running).Count();
            var currentRunningTasks = ApplicationState.NonUpdateRunningTasks(this);
            var outputString = "";

            if (dirtySourceCount > 0)
            {
                var dirtySources = context.Sources.Where(so => so.SiteID == site.SiteID && so.Dirty && !so.Running);
                context.Sources.RemoveRange(dirtySources);

                outputString += $">>> [{DateTime.Now.ToString(Helpers.LogTimeFormat)}] MangaReader: update - removing {dirtySources.Count()} dirty entries\n";
                context.SaveChanges();
            }

            if (runningSourceCount > 0 && currentRunningTasks == 0)
            {
                var dbSources = context.Sources.Where(so => so.SiteID == site.SiteID && so.Running && !so.Dirty);
                foreach (var dbSource in dbSources)
                {
                    dbSource.Running = false;
                }

                outputString += $">>> [{DateTime.Now.ToString(Helpers.LogTimeFormat)}] MangaReader: update - disabling {dbSources.Count()} running entries\n";
                context.SaveChanges();
            }

            // TODO only allows updates on day old sites < DateTime.Now.AddDays(-1)
            if (site.LastUpdated == null || DateTime.SpecifyKind(DateTime.Parse(site.LastUpdated), DateTimeKind.Utc) < DateTime.Now)
            {
                outputString += $">>> [{DateTime.Now.ToString(Helpers.LogTimeFormat)}] MangaReader: update - queuing initialize task\n";

                site.LastUpdated = DateTime.UtcNow.ToString(Helpers.DatabaseTimeFormat);

                context.SaveChanges();

                ApplicationState.CreateInitTask(SiteName.MangaReader);
            }

            Console.Write(outputString);

            ApplicationState.ReleaseSiteLock(this);
            ApplicationState.ReleaseSiteTBLock();
        }

        public static void ProcessTasks(ScraperTask scraperTask)
        {
            using (var context = new WebjobContext())
            {
                if (scraperTask.Type == ScraperTaskType.Initialize)
                {
                    Console.WriteLine($">>> Mangareader initialize - starttime: {DateTime.UtcNow.ToString(Helpers.DatabaseTimeFormat)}");

                    if (ApplicationState.InitSafeStart(task: scraperTask))
                    {
                        ISite mangaReader;

                        if ((mangaReader = ApplicationState.GetSite(scraperTask)) != null)
                        {
                            mangaReader.Initialize(context, scraperTask);

                            ApplicationState.RemoveTask(scraperTask);
                        }
                        else
                        {
                            Console.WriteLine($">>> Mangareader: init cancelled - no task exists in local cache");
                        }
                    }
                    else
                    {
                        Console.WriteLine($">>> Mangareader: init cancelled - multiple initialize tasks running");
                    }
                    Console.WriteLine($">>> Mangareader initialize - endtime: {DateTime.UtcNow.ToString(Helpers.DatabaseTimeFormat)}");
                }
                else if (scraperTask.Type == ScraperTaskType.DownloadDetails)
                {
                    Console.WriteLine($">>> Mangareader: download - starttime: {DateTime.UtcNow.ToString(Helpers.DatabaseTimeFormat)}");

                    ISite mangaReader;

                    if ((mangaReader = ApplicationState.GetSite(scraperTask)) != null)
                    {
                        mangaReader.DownloadDetails(context, scraperTask);
                    }
                    else
                    {
                        Console.WriteLine($">>> Mangareader: download cancelled");
                    }
                    Console.WriteLine($">>> MangaReader download - endtime: {DateTime.UtcNow.ToString(Helpers.DatabaseTimeFormat)}");
                    ApplicationState.RemoveTask(scraperTask);
                }
                else if (scraperTask.Type == ScraperTaskType.Update)
                {
                    Console.WriteLine($">>> Mangareader: update - starttime: {DateTime.UtcNow.ToString(Helpers.DatabaseTimeFormat)}");

                    ISite mangaReader = null;

                    if ((mangaReader = ApplicationState.GetSite(scraperTask)) != null)
                    {
                        mangaReader.Update(context, scraperTask);
                    }
                    else
                    {
                        mangaReader = new MangaReader();

                        if (ApplicationState.PutSite(scraperTask, mangaReader))
                        {
                            mangaReader.Update(context, scraperTask);
                        }
                        else
                        {
                            Console.WriteLine($">>> Mangareader: update cancelled");
                        }
                    }
                    Console.WriteLine($">>> Mangareader update - endtime: {DateTime.UtcNow.ToString(Helpers.DatabaseTimeFormat)}");
                    ApplicationState.RemoveTask(scraperTask);
                }
            }
        }

        public void Cancel()
        {
            isCancelled = true;
        }
    }
}