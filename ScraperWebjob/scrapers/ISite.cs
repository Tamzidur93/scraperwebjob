﻿using ScraperWebjob.db;
using ScraperWebjob.scrapers.tasks;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScraperWebjob.scrapers
{
    internal interface ISite
    {
        void Initialize(WebjobContext context, ScraperTask scraperTask); // init
        void DownloadDetails(WebjobContext context, ScraperTask scraperTask); // download
        void Update(WebjobContext context, ScraperTask scraperTask); // update
        void Cancel();

        //public enum ScraperTaskType
        //{
        //    Initialize, Download, Update, Clean
        //}
    }
}


// Initialize, Download, CheckAge, Update, Refresh