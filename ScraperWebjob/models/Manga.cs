﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScraperWebjob.models
{
    class Manga
    {

        private Manga()
        {
        }        

        public Manga(string name, string lastModified)
        {
            Name = name;
            LastModified = lastModified;
        }

        [Key]
        public int MangaID { get; set; }

        [Index(IsUnique = true), StringLength(450), Required(AllowEmptyStrings = false)]
        public string Name { get; set; }

        [Required]
        public string LastModified { get; set; }

        public ICollection<Source> Sources { get; set; }

        public override bool Equals(object obj)
        {
            var item = obj as Manga;

            if (item == null || GetType() != item.GetType())
            {
                return false;
            }

            return Name.ToLower().Trim().Equals(item.Name.ToLower().Trim());    
        }

        public override int GetHashCode()
        {
            return 0;
        }

    }
}
