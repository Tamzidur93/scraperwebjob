﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScraperWebjob.models
{
    class Detail
    {

        private Detail()
        {

        }

        public Detail(
            string alternateNames, 
            string yearOfRelease, 
            string status, 
            string authors, 
            string artists, 
            string imageURL, 
            string summary, 
            string genre, 
            string lastModified,
            Source source
            )
        {
            AlternateNames = alternateNames;
            YearOfRelease = yearOfRelease;
            Status = status;
            Authors = authors;
            Artists = artists;
            ImageURL = imageURL;
            Summary = summary;
            Genre = genre;
            Source = source;
            LastModified = lastModified;
        }
        
        [Key, ForeignKey("Source")]
        public int DetailID { get; set; }
        [Required, Index(IsUnique = true)]
        public Source Source { get; set; }

        [Required (AllowEmptyStrings = true)]
        public string AlternateNames { get; set; }
        [Required(AllowEmptyStrings = true)]
        public string YearOfRelease{ get; set; }
        [Required(AllowEmptyStrings = true)]
        public string Status { get; set; }
        [Required(AllowEmptyStrings = true)]
        public string Authors { get; set; }
        [Required(AllowEmptyStrings = true)]
        public string Artists { get; set; }
        [Required(AllowEmptyStrings = true)]
        public string ImageURL{ get; set; }
        [Required(AllowEmptyStrings = true)]
        public string Summary { get; set; }
        [Required(AllowEmptyStrings = true)]
        public string Genre { get; set; }

        // never used
        [Required(AllowEmptyStrings = true)]
        public string LastModified { get; set; }

        public override int GetHashCode()
        {
            return 0;
        }

        public override bool Equals(object obj)
        {
            var item = obj as Detail;

            if (item == null || item.GetType() != GetType())
            {
                return false;
            }

            bool res = true;

            res = res && AlternateNames.ToLower().Equals(item.AlternateNames.ToLower());
            res = res && YearOfRelease.ToLower().Equals(item.YearOfRelease.ToLower());
            res = res && Status.ToLower().Equals(item.Status.ToLower());
            res = res && Authors.ToLower().Equals(item.Authors.ToLower());
            res = res && Artists.ToLower().Equals(item.Artists.ToLower());
            res = res && ImageURL.ToLower().Equals(item.ImageURL.ToLower());
            res = res && Summary.ToLower().Equals(item.Summary.ToLower());
            res = res && Genre.ToLower().Equals(item.Genre.ToLower());

            return res;
        }
    }
}
