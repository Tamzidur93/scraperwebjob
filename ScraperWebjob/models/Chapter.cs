﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScraperWebjob.models
{
    class Chapter
    {

        private Chapter()
        {

        }

        public Chapter(int chapterIndex, string chapterName, string chapterURL, Source source, string lastModified)
        {
            ChapterIndex = chapterIndex;
            ChapterName = chapterName;
            ChapterURL = chapterURL;
            SourceID = source.SourceID;
            Source = source;
            LastModified = lastModified;
        }

        [Key, Required]
        public int ChapterID { get; set; }

        [Required]
        public string ChapterName { get; set; }
        
        [Required]
        public string ChapterURL{ get; set; }

        [Required, Index(name: "IX_Unique_Source", order: 1, IsUnique = true)]
        public int ChapterIndex { get; set; }

        [Required, ForeignKey("Source"), Index(name: "IX_Unique_Source", order:0, IsUnique = true)]
        public int SourceID { get; set; }

        [Required]
        public Source Source { get; set; }

        [Required]
        public string LastModified { get; set; }

        public ICollection<PageLink> PageLinks { get; set; }

        [Required]
        public bool Dirty { get; set; } = false;

        public override int GetHashCode()
        {
            return 0;
        }

        public override bool Equals(object obj)
        {
            var item = obj as Chapter;

            if(obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            return GetHashCode().Equals(item.GetHashCode())
                && ChapterIndex.Equals(item.ChapterIndex)
                && ChapterURL.ToLower().Trim().Equals(item.ChapterURL.ToLower().Trim())
                && Source.Equals(item.Source);
        }
    }
}
