﻿using ScraperWebjob.models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScraperWebjob.db
{

    class WebjobContext : DbContext
    {
        public WebjobContext() : base(nameOrConnectionString: "scraperDbConnString") { }

        public DbSet<Manga> Mangas { get; set; }
        public DbSet<Site> Sites { get; set; }
        public DbSet<Source> Sources { get; set; }
        public DbSet<Chapter> Chapters { get; set; }
        public DbSet<PageLink> PageLinks { get; set; }
        public DbSet<Detail> Details { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            ////Manga - Site mapping via Source
            //modelBuilder.Entity<Source>()
            //    .HasKey(so => new { so.MangaID, so.SiteID});



        }


        /*System.InvalidOperationException The DbConfiguration type 'MySql.Data.Entity.MySqlEFConfiguration, MySql.Data.Entity.EF6' 
        specified in the application config file could not be loaded. Make sure that the assembly-qualified 
        name is used and that the assembly is available to the running application. See 
        http://go.microsoft.com/fwlink/?LinkId=260883 for more information
        */



    }
}
