﻿using ScraperWebjob.db;
using ScraperWebjob.helpers;
using ScraperWebjob.models;
using ScraperWebjob.scrapers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using System.Configuration;
using Microsoft.Azure;
using Microsoft.Azure.WebJobs;
using ScraperWebjob.scrapers.tasks;
using Newtonsoft.Json;
using static ScraperWebjob.scrapers.tasks.ScraperTask;
using System.Collections.Concurrent;
using HtmlAgilityPack;
using System.Diagnostics;
using System.Data.Entity.Migrations;
using System.Threading;

namespace ScraperWebjob
{
    public class Program
    {

        public static void Main(String[] args)
        {
            //var context = new Context();
            //Helpers.ClearDatabase(context);
            //var mangaReader = new MangaReader();

            //CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings.Get("StorageConnectionString"));
            //CloudQueueClient queueClient = storageAccount.CreateCloudQueueClient();
            //CloudQueue queue = queueClient.GetQueueReference("scraperqueue");
            //queue.CreateIfNotExists();
            //CloudQueueMessage message = new CloudQueueMessage("this is the data I want to add to my message queue");
            //queue.AddMessage(message);

            //ApplicationState.StartScraper();

            //CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connectionString);
            //CloudQueueClient queueClient = storageAccount.CreateCloudQueueClient();
            //CloudQueue scraperQueue = queueClient.GetQueueReference("scraperqueue");

            //scraperQueue.CreateIfNotExists();

            //ApplicationState.CreateUpdateTask(SiteName.MangaReader);
            //ApplicationState.CreateUpdateTask(SiteName.MangaPanda);

            //var timer = new System.Threading.Timer(e => ApplicationState.CreateUpdateTask(SiteName.MangaReader), null, TimeSpan.Zero, TimeSpan.FromMinutes(3));

            //scraperQueue.AddMessage(updateTaskMessage2);

            // add messages to update queue every 4 hours

            //mangaReader.GetMangas(context);
            //mangaReader.GetChapters(context);
            //mangaReader.GetPages(context);

            //mangaReader.UpdateDatabase(context);

            //Helpers.ClearDatabase(context);

            //var utcTime = DateTime.Now.ToUniversalTime().ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'");

            //Console.WriteLine(utcTime);

            //var parsedTime = DateTime.SpecifyKind(DateTime.Parse(utcTime), DateTimeKind.Utc).ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss");

            //Console.WriteLine(parsedTime);

#if DEBUG
            var running = true;
            var dropdb = false;
            Console.WriteLine("COMMANDS: \n0 - exit \n1 - start jobhost \n2 - stop jobhost \n3 - update scrapers \n4 - clear scraperqueue \n5 - clear database");
            while (running)
            {
                var key = Console.ReadKey(true);

                switch (key.KeyChar)
                {
                    case '0':
                        Console.WriteLine("\tExiting");
                        running = false;
                        break;
                    case '1':
                        Console.WriteLine("\tStarting scraper jobhost");
                        ApplicationState.StartScraper();
                        break;
                    case '2':
                        Console.WriteLine("\tStopping scraper jobhost");
                        ApplicationState.StopScraper();
                        break;
                    case '3':
                        Console.WriteLine("\tGenerating tasks");
                        ApplicationState.GenerateTasks();
                        Console.WriteLine("\tTasks Generated");
                        break;
                    case '4':
                        Console.WriteLine("\tClearing scraperqueue");
                        Helpers.ClearScraperQueue();
                        Console.WriteLine("\tScraperqueue Cleared");
                        break;
                    case '5':
                        Console.WriteLine("CONFIRM COMMANDS: \n0 - drop database \n1 - return");
                        dropdb = true;
                        while (dropdb)
                        {
                            key = Console.ReadKey(true);
                            switch (key.KeyChar)
                            {
                                case '0':
                                    Console.WriteLine("\tClearing Database");
                                    Helpers.ClearDatabase(new WebjobContext());
                                    Console.WriteLine("\tDatabase Cleared");
                                    break;
                                case '1':
                                    break;
                            }
                            dropdb = false;
                            Console.WriteLine("COMMANDS: \n0 - exit \n1 - start jobhost \n2 - stop jobhost \n3 - update scrapers \n4 - clear scraperqueue \n5 - clear database");
                        }
                        break;
                    case 'h':
                        Console.WriteLine("COMMANDS: \n0 - exit \n1 - start jobhost \n2 - stop jobhost \n3 - update scrapers \n4 - clear scraperqueue \n5 - clear database");
                        break;
                }
            }
#else
            ApplicationState.StartScraper();       
#endif
            Console.ReadLine();
        }

        public static void ProcessQueueMessage([QueueTrigger("scraperqueue")] ScraperTask scraperTask)
        //public static void ProcessQueueMessage(ScraperTask scraperTask)
        {

            var runningTasks = ApplicationState.NonUpdateRunningTasks();

            // max tasks - amount of scrapers = 32-2
            if (runningTasks < 31 || scraperTask.Type.Equals(ScraperTaskType.Initialize) || scraperTask.Type.Equals(ScraperTaskType.Update))
            {            
                switch (scraperTask.SiteName)
                {
                    case SiteName.MangaReader:
                        MangaReader.ProcessTasks(scraperTask);
                        break;
                    case SiteName.MangaPanda:
                        MangaPanda.ProcessTasks(scraperTask);
                        break;
                }
            }
        }
    }
}


// system outline
// initialize -> download all content for all sites
// check for updates every 4 hours
// 

// process outlines
/* 
 * initialize
 *  download all mangas and sources for a site (completion time expected to be less than 4 hours)
 *  create new manga entries if not stored in database
 *  create new source if either no source is found for a manga or if manga is new
 *  update source if it is older than 4 days or if content doesnt match (source is marked as dirty in this case)
 *  delete source if it is not found in update set
 *  schedule n download tasks where n = (number of sources/ 500)
 * download
 *  downloads all chapters, details, pagelinks for each source (completion time expected to be more than 1 week)
 *  run on a set of sources provided by task parameters
 *  load all sources marked !dirty and !running
 *  mark all sources as active when procedure is running
 *  if a source already contains chapters 
 *      check if the number of pages on the website are same as stored version
 *          if same -> ignore
 *          if different redownload all pages for chapter
 *  if downloaded webpage has chapters not stored in database only download the new chapter and its pagelinks  
 * update
 *  scheduled to run every 4 hours
 *  if today - site.lastModified  > 14 days -> schedule initialize task 
 *  else check all sources' date and mark any dirty which have today - last modified > 4 days -> schedule refresh task if new dirty count > 0 (update task marked entries as dirty)
 *  
 * refresh 
 *  load all sources marked dirty and !running
 *  mark all loaded sources as running when procedure is running
 *  if a source already contains chapters 
 *      check if the number of pages on the website are same as stored version
 *          if same -> ignore
 *          if different redownload all pages for chapter
 *  if downloaded webpage has chapters not stored in database only download the new chapter and its pagelinks  
 *  *FIXED* notes: need to ensure only 1 refresh task is run at a time for a site - just check if there is already a refresh task in active task list
 *  notes: race condition on back to back tasks (down -> init, down -> refresh, refresh -> down), introduce shared repository and database locking
 *  notes: need to check if application is disposed from other threads - current case = webjob can be shut down while other tasks are looking for it
 */
