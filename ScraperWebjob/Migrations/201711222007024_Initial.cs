namespace ScraperWebjob.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Chapters",
                c => new
                    {
                        ChapterID = c.Int(nullable: false, identity: true),
                        ChapterName = c.String(nullable: false, unicode: false),
                        ChapterURL = c.String(nullable: false, unicode: false),
                        ChapterIndex = c.Int(nullable: false),
                        SourceID = c.Int(nullable: false),
                        LastModified = c.String(nullable: false, unicode: false),
                        Dirty = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ChapterID)                
                .ForeignKey("Sources", t => t.SourceID, cascadeDelete: true)
                .Index(t => new { t.SourceID, t.ChapterIndex }, unique: true, name: "IX_Unique_Source");
            
            CreateTable(
                "PageLinks",
                c => new
                    {
                        PageLinkID = c.Int(nullable: false, identity: true),
                        PageIndex = c.Int(nullable: false),
                        ChapterID = c.Int(nullable: false),
                        ImageURL = c.String(nullable: false, unicode: false),
                        PageURL = c.String(nullable: false, unicode: false),
                    })
                .PrimaryKey(t => t.PageLinkID)                
                .ForeignKey("Chapters", t => t.ChapterID, cascadeDelete: true)
                .Index(t => new { t.ChapterID, t.PageIndex }, unique: true, name: "IX_PageIndexFK_Unique");
            
            CreateTable(
                "Sources",
                c => new
                    {
                        SourceID = c.Int(nullable: false, identity: true),
                        SourceURL = c.String(nullable: false, unicode: false),
                        MangaID = c.Int(nullable: false),
                        SiteID = c.Int(nullable: false),
                        LastModified = c.String(nullable: false, unicode: false),
                        Dirty = c.Boolean(nullable: false),
                        Running = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.SourceID)                
                .ForeignKey("Mangas", t => t.MangaID, cascadeDelete: true)
                .ForeignKey("Sites", t => t.SiteID, cascadeDelete: true)
                .Index(t => new { t.SiteID, t.MangaID }, unique: true, name: "IX_SiteMap_Unique");
            
            CreateTable(
                "Details",
                c => new
                    {
                        DetailID = c.Int(nullable: false),
                        AlternateNames = c.String(nullable: false, unicode: false),
                        YearOfRelease = c.String(nullable: false, unicode: false),
                        Status = c.String(nullable: false, unicode: false),
                        Authors = c.String(nullable: false, unicode: false),
                        Artists = c.String(nullable: false, unicode: false),
                        ImageURL = c.String(nullable: false, unicode: false),
                        Summary = c.String(nullable: false, unicode: false),
                        Genre = c.String(nullable: false, unicode: false),
                        LastModified = c.String(nullable: false, unicode: false),
                    })
                .PrimaryKey(t => t.DetailID)                
                .ForeignKey("Sources", t => t.DetailID)
                .Index(t => t.DetailID);
            
            CreateTable(
                "Mangas",
                c => new
                    {
                        MangaID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 450, storeType: "nvarchar"),
                        LastModified = c.String(nullable: false, unicode: false),
                    })
                .PrimaryKey(t => t.MangaID)                
                .Index(t => t.Name, unique: true);
            
            CreateTable(
                "Sites",
                c => new
                    {
                        SiteID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 450, storeType: "nvarchar"),
                        SiteURL = c.String(nullable: false, unicode: false),
                        LastModified = c.String(nullable: false, unicode: false),
                        LastUpdated = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.SiteID)                
                .Index(t => t.Name, unique: true);
            
        }
        
        public override void Down()
        {
            DropForeignKey("Chapters", "SourceID", "Sources");
            DropForeignKey("Sources", "SiteID", "Sites");
            DropForeignKey("Sources", "MangaID", "Mangas");
            DropForeignKey("Details", "DetailID", "Sources");
            DropForeignKey("PageLinks", "ChapterID", "Chapters");
            DropIndex("Sites", new[] { "Name" });
            DropIndex("Mangas", new[] { "Name" });
            DropIndex("Details", new[] { "DetailID" });
            DropIndex("Sources", "IX_SiteMap_Unique");
            DropIndex("PageLinks", "IX_PageIndexFK_Unique");
            DropIndex("Chapters", "IX_Unique_Source");
            DropTable("Sites");
            DropTable("Mangas");
            DropTable("Details");
            DropTable("Sources");
            DropTable("PageLinks");
            DropTable("Chapters");
        }
    }
}
