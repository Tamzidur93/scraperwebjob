﻿using Microsoft.Azure.WebJobs;
using Microsoft.WindowsAzure.Storage.Queue;
using Newtonsoft.Json;
using ScraperWebjob.scrapers;
using ScraperWebjob.scrapers.tasks;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static ScraperWebjob.scrapers.tasks.ScraperTask;

namespace ScraperWebjob.helpers
{
    public static class ApplicationState
    {
        private static JobHost host;

        private static ConcurrentDictionary<ScraperTask, ISite> MangaReaderActiveTasks { get; } = new ConcurrentDictionary<ScraperTask, ISite>();
        private static ConcurrentDictionary<ScraperTask, ISite> MangaPandaActiveTasks { get; } = new ConcurrentDictionary<ScraperTask, ISite>();

        private static Semaphore mangaReaderDBSemaphore = new Semaphore(1, 1);
        private static Semaphore mangaPandaDBSemaphore = new Semaphore(1, 1);
        private static Semaphore mangaFoxDbSemaphore = new Semaphore(1, 1);
        private static Semaphore mangaTbSemaphore = new Semaphore(1, 1);
        private static Semaphore siteTbSemaphore = new Semaphore(1, 1);

        private static int mangaReaderTaskIndex = 0;
        private static int mangaPandaTaskIndex = 0;

        private static Semaphore initCountSemaphore = new Semaphore(1, 1);
        private static int initCounter = 0;

        public static int CurrentInitTasks(ScraperTask task)
        {
            int count = 0;
            switch (task.SiteName)
            {
                case SiteName.MangaReader:
                    lock (MangaReaderActiveTasks)
                    {
                        count = MangaReaderActiveTasks.Where(taskKV => taskKV.Key.Type == ScraperTaskType.Initialize).Count();
                    }
                    break;
                case SiteName.MangaPanda:
                    lock (MangaPandaActiveTasks)
                    {
                        count = MangaPandaActiveTasks.Where(taskKV => taskKV.Key.Type == ScraperTaskType.Initialize).Count();
                    }
                    break;
                default:
                    throw new NotImplementedException();
            }
            return count;
        }

        internal static bool InitSafeStart(ScraperTask task)
        {
            bool result = false;
            switch (task.SiteName)
            {
                case SiteName.MangaReader:
                    lock (MangaReaderActiveTasks)
                    {
                        var currentInitTasks = MangaReaderActiveTasks.Where(taskKV => taskKV.Key.Type.Equals(ScraperTaskType.Initialize));

                        if (currentInitTasks.Count() != 1)
                        {
                            ISite site;
                            MangaReaderActiveTasks.TryRemove(task, out site);

                            if (site != null)
                            {
                                site.Cancel();
                            }

                            result = false;
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;
                case SiteName.MangaPanda:
                    lock (MangaPandaActiveTasks)
                    {
                        var currentInitTasks = MangaPandaActiveTasks.Where(taskKV => taskKV.Key.Type.Equals(ScraperTaskType.Initialize));

                        if (currentInitTasks.Count() != 1)
                        {
                            ISite site;
                            MangaPandaActiveTasks.TryRemove(task, out site);

                            if (site != null)
                            {
                                site.Cancel();
                            }

                            result = false;
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    break;
                default:
                    throw new NotImplementedException();
            }
            return result;
        }

        internal static void DecrementInitCounter()
        {
            initCountSemaphore.WaitOne();
            initCounter--;
            Console.WriteLine("current running init tasks:" + initCounter);
            initCountSemaphore.Release();
        }

        internal static void IncrementInitCounter()
        {
            initCountSemaphore.WaitOne();
            initCounter++;
            Console.WriteLine("current running init tasks:" + initCounter);
            initCountSemaphore.Release();
        }

        internal static void CreateInitTask(SiteName site)
        {
            switch (site)
            {
                case SiteName.MangaReader:
                    lock (MangaReaderActiveTasks)
                    {
                        var initTask = new ScraperTask(site: site, type: ScraperTaskType.Initialize, taskIndex: mangaReaderTaskIndex);
                        mangaReaderTaskIndex = (mangaReaderTaskIndex + 1) % 1024;
                        var initTaskMessage = new CloudQueueMessage(JsonConvert.SerializeObject(initTask));
                        var scraperQueue = Helpers.GetScraperQueueInstance();

                        var res = MangaReaderActiveTasks.TryAdd(initTask, new MangaReader());

                        if (res)
                        {
                            scraperQueue.AddMessageAsync(initTaskMessage);
                        }
                        else
                        {
                            Console.WriteLine("unable to create init task");
                        }
                    }
                    break;
                case SiteName.MangaPanda:
                    lock (MangaPandaActiveTasks)
                    {
                        var initTask = new ScraperTask(site: site, type: ScraperTaskType.Initialize, taskIndex: mangaPandaTaskIndex);
                        mangaPandaTaskIndex = (mangaPandaTaskIndex + 1) % 1024;
                        var initTaskMessage = new CloudQueueMessage(JsonConvert.SerializeObject(initTask));
                        var scraperQueue = Helpers.GetScraperQueueInstance();

                        var res = MangaPandaActiveTasks.TryAdd(initTask, new MangaPanda());

                        if (res)
                        {
                            scraperQueue.AddMessageAsync(initTaskMessage);
                        }
                        else
                        {
                            Console.WriteLine("unable to create init task");
                        }
                    }
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        internal static void CreateDownloadTask(SiteName site, string parameters)
        {
            switch (site)
            {
                case SiteName.MangaReader:
                    lock (MangaReaderActiveTasks)
                    {
                        var downloadTask = new ScraperTask(siteName: site, type: ScraperTaskType.DownloadDetails, taskIndex: mangaReaderTaskIndex, parameters: parameters);
                        mangaReaderTaskIndex = (mangaReaderTaskIndex + 1) % 1024;
                        var downloadTaskMessage = new CloudQueueMessage(JsonConvert.SerializeObject(downloadTask));
                        var scraperQueue = Helpers.GetScraperQueueInstance();

                        var mangaReader = new MangaReader();

                        var res = MangaReaderActiveTasks.TryAdd(downloadTask, mangaReader);

                        if (res)
                        {
                            scraperQueue.AddMessage(downloadTaskMessage);
                        }
                        else
                        {
                            Console.WriteLine("unable to create download task");
                        }
                    }
                    break;
                case SiteName.MangaPanda:
                    lock (MangaPandaActiveTasks)
                    {
                        var downloadTask = new ScraperTask(siteName: site, type: ScraperTaskType.DownloadDetails, taskIndex: mangaPandaTaskIndex, parameters: parameters);
                        mangaPandaTaskIndex = (mangaPandaTaskIndex + 1 % 1024);
                        var downloadTaskMessage = new CloudQueueMessage(JsonConvert.SerializeObject(downloadTask));
                        var scraperQueue = Helpers.GetScraperQueueInstance();

                        var res = MangaPandaActiveTasks.TryAdd(downloadTask, new MangaPanda());

                        if (res)
                        {
                            scraperQueue.AddMessage(downloadTaskMessage);
                        }
                        else
                        {
                            Console.WriteLine("unable to create download task");
                        }
                    }
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        internal static void CreateUpdateTask(SiteName site)
        {
            switch (site)
            {
                case SiteName.MangaReader:
                    lock (MangaReaderActiveTasks)
                    {
                        var updateTask = new ScraperTask(site: site, type: ScraperTaskType.Update, taskIndex: mangaReaderTaskIndex);
                        mangaReaderTaskIndex = (mangaReaderTaskIndex + 1) % 1024;
                        var updateTaskMessage = new CloudQueueMessage(JsonConvert.SerializeObject(updateTask));
                        var scraperQueue = Helpers.GetScraperQueueInstance();

                        var res = MangaReaderActiveTasks.TryAdd(updateTask, new MangaReader());

                        if (res)
                        {
                            scraperQueue.AddMessage(updateTaskMessage);
                        }
                        else
                        {
                            Console.WriteLine("unable to create update task");
                        }
                    }
                    break;
                case SiteName.MangaPanda:
                    lock (MangaPandaActiveTasks)
                    {
                        var updateTask = new ScraperTask(site: site, type: ScraperTaskType.Update, taskIndex: mangaPandaTaskIndex);
                        mangaPandaTaskIndex = (mangaPandaTaskIndex + 1) % 1024;
                        var updateTaskMessage = new CloudQueueMessage(JsonConvert.SerializeObject(updateTask));
                        var scraperQueue = Helpers.GetScraperQueueInstance();

                        var res = MangaPandaActiveTasks.TryAdd(updateTask, new MangaPanda());

                        if (res)
                        {
                            scraperQueue.AddMessage(updateTaskMessage);
                        }
                        else
                        {
                            Console.WriteLine("unable to create update task");
                        }
                    }
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        internal static void RemoveTask(ScraperTask scraperTask)
        {
            switch (scraperTask.SiteName)
            {
                case SiteName.MangaReader:
                    lock (MangaReaderActiveTasks)
                    {
                        ISite site;
                        MangaReaderActiveTasks.TryRemove(scraperTask, out site);
                        if (site != null)
                        {
                            site.Cancel();
                        }
                    }
                    break;
                case SiteName.MangaPanda:
                    lock (MangaPandaActiveTasks)
                    {
                        ISite site;
                        MangaPandaActiveTasks.TryRemove(scraperTask, out site);
                        if (site != null)
                        {
                            site.Cancel();
                        }
                    }
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        internal static void AcquireSiteLock(ISite site)
        {
            if (site is MangaReader)
            {
                mangaReaderDBSemaphore.WaitOne();
            }
            else if (site is MangaPanda)
            {
                mangaPandaDBSemaphore.WaitOne();
            }
        }

        internal static void ReleaseSiteLock(ISite site)
        {
            if (site is MangaReader)
            {
                mangaReaderDBSemaphore.Release();
            }
            else if (site is MangaPanda)
            {
                mangaPandaDBSemaphore.Release();
            }
        }

        internal static void AcquireMangaTBLock()
        {
            mangaTbSemaphore.WaitOne();
        }

        internal static void ReleaseMangaTBLock()
        {
            mangaTbSemaphore.Release();
        }

        internal static void AcquireSiteTBLock()
        {
            siteTbSemaphore.WaitOne();
        }

        internal static void ReleaseSiteTBLock()
        {
            siteTbSemaphore.Release();
        }

        internal static ISite GetSite(ScraperTask scraperTask)
        {
            ISite site = null;

            switch (scraperTask.SiteName)
            {
                case SiteName.MangaReader:
                    MangaReaderActiveTasks.TryGetValue(scraperTask, out site);
                    break;
                case SiteName.MangaPanda:
                    MangaPandaActiveTasks.TryGetValue(scraperTask, out site);
                    break;
                default:
                    throw new NotImplementedException();
            }

            return site;
        }

        internal static bool PutSite(ScraperTask scraperTask, ISite site)
        {
            var res = false;

            switch (scraperTask.SiteName)
            {
                case SiteName.MangaReader:
                    res = MangaReaderActiveTasks.TryAdd(scraperTask, site);
                    break;
                case SiteName.MangaPanda:
                    res = MangaPandaActiveTasks.TryAdd(scraperTask, site);
                    break;
                default:
                    throw new NotImplementedException();
            }

            return res;
        }

        internal static void GenerateTasks()
        {
            CreateUpdateTask(SiteName.MangaReader);
            CreateUpdateTask(SiteName.MangaPanda);
        }

        internal static void StartScraper()
        {
            var connectionString = ConfigurationManager.AppSettings.Get("StorageConnectionString");
            var config = new JobHostConfiguration(connectionString);
            config.Queues.BatchSize = 32;
            host = new JobHost(config);

            var configuration = new Migrations.Configuration();
            var migrator = new DbMigrator(configuration);
            migrator.Update();

            //CancellationTokenSource cts = new CancellationTokenSource(TimeSpan.FromMinutes(2));
            //var cancellationToken = cts.Token;

            var cancellationToken = new WebJobsShutdownWatcher().Token;

            cancellationToken.Register(StopScraper);

#if DEBUG
            var timer = new Timer((e) => GenerateTasks(), null, TimeSpan.Zero, TimeSpan.FromHours(4));
#endif

            Console.WriteLine($">>> [{DateTime.Now.ToString(Helpers.LogTimeFormat)}] Scraper jobhost started");

            host.StartAsync();
        }

        internal static void StopScraper()
        {
            lock (MangaReaderActiveTasks)
            {
                foreach (var taskKV in MangaReaderActiveTasks)
                {
                    taskKV.Value.Cancel();
                }
            }
            lock (MangaPandaActiveTasks)
            {
                foreach (var taskKV in MangaPandaActiveTasks)
                {
                    taskKV.Value.Cancel();
                }
            }

            host.Stop();

            MangaReaderActiveTasks.Clear();
            MangaPandaActiveTasks.Clear();

            Console.WriteLine($">>> [{DateTime.Now.ToString(Helpers.LogTimeFormat)}] Scraper jobhost stopped");

            // lock and shutdown others
        }

        internal static int NonUpdateRunningTasks(ISite site)
        {
            if (site is MangaReader)
            {
                lock (MangaReaderActiveTasks)
                {
                    return MangaReaderActiveTasks.Where(t => t.Key.Type != ScraperTaskType.Update).Count();
                }
            }
            else if (site is MangaPanda)
            {
                lock (MangaPandaActiveTasks)
                {
                    return MangaPandaActiveTasks.Where(t => t.Key.Type != ScraperTaskType.Update).Count();
                }
            }
            return -1;
        }

        internal static int NonUpdateRunningTasks()
        {
            int res = 0;

            lock(MangaReaderActiveTasks)
            {
                res += MangaReaderActiveTasks.Count;
            }

            lock (MangaPandaActiveTasks)
            {
                res += MangaPandaActiveTasks.Count;
            }

            return res;
        }
    }
}
