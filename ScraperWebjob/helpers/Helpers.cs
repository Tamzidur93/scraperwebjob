﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using ScraperWebjob.db;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScraperWebjob.helpers
{
    static class Helpers
    {

        public static string DatabaseTimeFormat { get; } = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'";
        public static string LogTimeFormat { get; } = "yy'-'MM'-'dd'T'HH':'mm";

        internal static void ClearDatabase(WebjobContext context)
        {
            context.Mangas.RemoveRange(context.Mangas);
            context.Sites.RemoveRange(context.Sites);
            context.Sources.RemoveRange(context.Sources);
            context.Details.RemoveRange(context.Details);
            context.Chapters.RemoveRange(context.Chapters);
            context.PageLinks.RemoveRange(context.PageLinks);

            context.SaveChanges();
        }

        internal static void PrintErrors(Exception e)
        {
            Console.WriteLine(e.StackTrace);
            while (e != null)
            {
                Console.WriteLine(e.Message);
                e = e.InnerException;
            }
        }

        internal static void ClearScraperQueue()
        {
            var cloudQueue = GetScraperQueueInstance();
            cloudQueue.Clear();   
        }

        internal static CloudQueue GetScraperQueueInstance()
        {
            var connectionString = ConfigurationManager.AppSettings.Get("StorageConnectionString");

            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connectionString);
            CloudQueueClient queueClient = storageAccount.CreateCloudQueueClient();
            CloudQueue scraperQueue = queueClient.GetQueueReference("scraperqueue");

            scraperQueue.CreateIfNotExists();

            return scraperQueue;
        }

        internal static CloudQueue GetUpdateQueueInstance()
        {
            var connectionString = ConfigurationManager.AppSettings.Get("StorageConnectionString");

            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connectionString);
            CloudQueueClient queueClient = storageAccount.CreateCloudQueueClient();
            CloudQueue scraperQueue = queueClient.GetQueueReference("updatequeue");

            scraperQueue.CreateIfNotExists();

            return scraperQueue;
        }

        internal static string AppendUpdateFreqSequence(string[] values, int next)
        {
            var output = "";
            var sequenceLimit = 4;

            for (int i = (values.Length == sequenceLimit) ? 1 : 0; i < values.Length; i++)
            {
                output += values[i] + ",";
            }

            output += next;
            return output;
        }

    }
}
