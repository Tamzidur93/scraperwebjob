﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using ScraperWebjobV2.Misc;
using System.IO;
using ScraperWebjobV2.Scrapers.Tasks;
using ScraperWebjobV2.Db;
using static ScraperWebjobV2.Scrapers.Tasks.ScraperTask;

namespace ScraperWebjobV2
{
    // To learn more about Microsoft Azure WebJobs SDK, please see https://go.microsoft.com/fwlink/?LinkID=320976
    public class Program
    {
        // Please set the following connection strings in app.config for this WebJob to run:
        // AzureWebJobsDashboard and AzureWebJobsStorage
        static void Main()
        {

#if DEBUG
            var running = true;
            var dropdb = false;
            Console.WriteLine("COMMANDS: \n0 - exit \n1 - start jobhost \n2 - stop jobhost \n3 - update scrapers \n4 - clear scraperqueue \n5 - clear database");
            while (running)
            {
                var key = Console.ReadKey(true);

                switch (key.KeyChar)
                {
                    case '0':
                        Console.WriteLine("\tExiting");
                        running = false;
                        break;
                    case '1':
                        Console.WriteLine("\tStarting scraper jobhost");
                        ApplicationState.StartScraper();
                        break;
                    case '2':
                        Console.WriteLine("\tStopping scraper jobhost");
                        ApplicationState.StopScraper();
                        break;
                    case '3':
                        Console.WriteLine("\tGenerating tasks");
                        ApplicationState.GenerateTasks();
                        Console.WriteLine("\tTasks Generated");
                        break;
                    case '4':
                        Console.WriteLine("\tClearing scraperqueue");
                        Helpers.ClearScraperQueue();
                        Console.WriteLine("\tScraperqueue Cleared");
                        break;
                    case '5':
                        Console.WriteLine("CONFIRM COMMANDS: \n0 - drop database \n1 - return");
                        dropdb = true;
                        while (dropdb)
                        {
                            key = Console.ReadKey(true);
                            switch (key.KeyChar)
                            {
                                case '0':
                                    Console.WriteLine("\tClearing Database");
                                    using(var context = new WebjobContext())
                                    {
                                        Helpers.ClearDatabase(new WebjobContext());
                                    }                                    
                                    Console.WriteLine("\tDatabase Cleared");
                                    break;
                                case '1':
                                    break;
                            }
                            dropdb = false;
                            Console.WriteLine("COMMANDS: \n0 - exit \n1 - start jobhost \n2 - stop jobhost \n3 - update scrapers \n4 - clear scraperqueue \n5 - clear database");
                        }
                        break;
                    case 'h':
                        Console.WriteLine("COMMANDS: \n0 - exit \n1 - start jobhost \n2 - stop jobhost \n3 - update scrapers \n4 - clear scraperqueue \n5 - clear database");
                        break;
                }
            }
#else
            ApplicationState.StartScraper();
#endif
            Console.ReadLine();
        }

        public static void ProcessQueueMessage([QueueTrigger("scraperqueue")] ScraperTask task, TextWriter logger)
        {
            var runningTasks = ApplicationState.RunningTaskHandlers();
            if (runningTasks >= 30 && task.Type.Equals(ScraperTaskType.DownloadDetails))
            {
                // requeue task
                ApplicationState.CreateDelayedTask(task, TimeSpan.FromHours(8));
                return;
            }

            AbstractTaskHandler taskHandler = null;
            switch (task.Type)
            {
                case ScraperTaskType.Initialize:
                    taskHandler = new InitializeTaskHandler(logger);
                    break;
                case ScraperTaskType.DownloadDetails:
                    taskHandler = new DownloadTaskHandler(logger);
                    break;
                case ScraperTaskType.Update:
                    taskHandler = new UpdateTaskHandler(logger);
                    break;
            }
            if (taskHandler != null)
            {
                ApplicationState.AddTask(taskHandler);
                taskHandler.HandleTask(task);
            }
        }
    }
}
