﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ScraperWebjobV2.Db;
using ScraperWebjobV2.Models;
using ScraperWebjobV2.Scrapers;
using ScraperWebjobV2.Scrapers.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ScraperWebjobV2.Scrapers.Tasks.ScraperTask;

namespace ScraperWebjobV2.Tests
{
    [TestClass]
    public class DownloadTaskTester
    {
        [TestMethod]
        public void TestGetSources()
        {
            var downloadTask = new DownloadTaskHandler(Console.Out);
            var sources = new List<Source>();

            using (var context = new WebjobContext())
            {
                try
                {
                    var scraperTask = new ScraperTask(SiteName.MangaReader, ScraperTaskType.DownloadDetails);
                    var scraperInstance = new MangaReader();
                    var site = downloadTask.GetSite(context, scraperTask, scraperInstance);

                    sources = downloadTask.GetSources(context: context, startIndex: 0, endIndex: 4, site: site, task: scraperTask);

                    Assert.AreEqual(4, sources.Count);
                }
                finally
                {
                    foreach (var source in sources)
                    {
                        source.Running = false;
                    }

                    context.SaveChanges();
                }
            }
        }
    }
}
