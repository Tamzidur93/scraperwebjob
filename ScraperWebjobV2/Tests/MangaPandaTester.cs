﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ScraperWebjobV2.Db;
using ScraperWebjobV2.Models;
using ScraperWebjobV2.Scrapers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScraperWebjobV2.Tests
{
    [TestClass]
    public class MangaPandaTester
    {
        [TestMethod]
        public void TestGetPageLinks()
        {
            var scraper = new MangaPanda();
            using (var context = new WebjobContext())
            {
                var chapter = new Chapter(
                    chapterIndex: 1,
                    chapterName: "",
                    chapterURL: "/kami-sama-no-joker/1",
                    source: new Source("", new Manga("", ""), new Site("", "", ""), ""),
                    lastModified: ""
                    );

                var list = scraper.GetPageLinks(chapter, Console.Out);

                Assert.AreEqual(47, list.Count);               
            }
        }
    }
}
