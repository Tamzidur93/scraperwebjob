﻿using System;
using System.Collections.Generic;
using System.Linq;
using ScraperWebjobV2.Models;
using HtmlAgilityPack;
using ScraperWebjobV2.Misc;
using System.Net;
using System.IO;

namespace ScraperWebjobV2.Scrapers
{
    class MangaReader : IScraper
    {
        private bool running = true;
        private const SiteName SITE_NAME = SiteName.MangaReader;
        private const string BASE_URL = "http://www.mangareader.net";
        private Random r = new Random();

        public bool Running { get => running; set => running = value; }
        public SiteName SiteName { get => SITE_NAME; }
        public string BaseURL { get => BASE_URL; }

        public List<Source> GetSources(Site site, TextWriter logger)
        {
            try
            {
                var web = new HtmlWeb();
                var mangaListURL = "/alphabetical";
                var doc = web.Load(BASE_URL + mangaListURL);
                var sourceList = new List<Source>();
                var utcTimeNow = Helpers.GetUtcTimeNowDB();
                var listItems = doc.DocumentNode.SelectNodes("//div[@class='series_col']//li");

                System.Threading.Thread.Sleep(1000 * r.Next(1, 5));

                if (web.StatusCode != HttpStatusCode.OK)
                {
                    throw new WebLoadException($"{SiteName.ToString()} scraper:{BASE_URL}+{mangaListURL} is incorrect");
                }

                foreach (var val in listItems)
                {
                    string mangaName = val.FirstChild.InnerHtml.Trim();
                    string mangaUrl = val.FirstChild.Attributes["href"].Value.Trim();

                    var manga = new Manga(name: mangaName, lastModified: utcTimeNow);
                    var source = new Source(sourceURL: mangaUrl, manga: manga, site: site, lastModified: utcTimeNow);

                    sourceList.Add(source);
                }

                return sourceList;
            }
            catch (WebLoadException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                Helpers.PrintErrors(e, logger);
                return null;
            }
        }

        public List<Chapter> GetChapters(Source source, TextWriter logger)
        {
            try
            {
                var web = new HtmlWeb();
                var doc = web.Load(BASE_URL + source.SourceURL);
                var chapterList = new List<Chapter>();
                var utcTimeNow = Helpers.GetUtcTimeNowDB();
                var createList = new List<Chapter>();

                System.Threading.Thread.Sleep(1000 * r.Next(1, 5));

                if (web.StatusCode != HttpStatusCode.OK)
                {
                    throw new WebLoadException($"{SiteName.ToString()} scraper:{BASE_URL}+{source.SourceURL} is incorrect - source is dirty");
                }

                var chapterElements = doc.DocumentNode.SelectNodes("//table[@id='listing']//tr[not(contains(@class, 'table_head'))]//a");

                for (int j = 0; j < chapterElements.Count; j++)
                {
                    var element = chapterElements[j];
                    var chapterName = element.InnerText.Trim();
                    var chapterURL = element.Attributes["href"].Value.Trim();
                    int index = j + 1;

                    var chapter = new Chapter(chapterIndex: index, chapterName: chapterName, chapterURL: chapterURL, source: source, lastModified: utcTimeNow);
                    createList.Add(chapter);
                }

                return createList;
            }
            catch (WebLoadException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                Helpers.PrintErrors(e, logger);
                return null;
            }
        }

        public Detail GetDetails(Source source, TextWriter logger)
        {
            try
            {
                var web = new HtmlWeb();
                var doc = web.Load(BASE_URL + source.SourceURL);
                var utcTimeNow = Helpers.GetUtcTimeNowDB();

                System.Threading.Thread.Sleep(1000 * r.Next(1, 5));

                if (web.StatusCode != HttpStatusCode.OK)
                {
                    throw new WebLoadException($"{SiteName.ToString()} scraper:{BASE_URL}+{source.SourceURL} is incorrect - source is dirty");
                }

                var detailElements = doc.DocumentNode.SelectNodes("//div[@id='mangaproperties']/table//tr//td"); // (key - value) map format -> stride = 2

                if (detailElements.Count % 2 != 0)
                {
                    throw new Exception("MangaPanda: detail template has changed");
                }

                Detail detail;

                string alternateName = null;
                string yearOfRelease = null;
                string status = null;
                string author = null;
                string artist = null;
                string genre = null;

                for (int j = 0; j < detailElements.Count(); j += 2)
                {
                    var element = detailElements[j];

                    var key = element.InnerText.Trim();

                    switch (key)
                    {
                        case "Alternate Name:":
                            alternateName = detailElements[j + 1].InnerText.Trim();
                            break;
                        case "Year of Release:":
                            yearOfRelease = detailElements[j + 1].InnerText.Trim();
                            break;
                        case "Status:":
                            status = detailElements[j + 1].InnerText.Trim();
                            break;
                        case "Author:":
                            author = detailElements[j + 1].InnerText.Trim();
                            break;
                        case "Artist:":
                            artist = detailElements[j + 1].InnerText.Trim();
                            break;
                        case "Genre:":
                            genre = detailElements[j + 1].InnerText.Trim().Replace(" ", ",");
                            break;
                    }
                }

                var mangaSum = doc.DocumentNode.SelectSingleNode("//div[@id='readmangasum']/p").InnerText.Trim();
                var mangaImgURL = doc.DocumentNode.SelectSingleNode("//*[@id='mangaimg']/img").Attributes["src"].Value.Trim();

                detail = new Detail(
                    alternateNames: alternateName,
                    yearOfRelease: yearOfRelease,
                    status: status,
                    authors: author,
                    artists: artist,
                    imageURL: mangaImgURL,
                    summary: mangaSum,
                    genre: genre,
                    lastModified: utcTimeNow,
                    source: source
                    );

                return detail;
            }
            catch (WebLoadException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                Helpers.PrintErrors(e, logger);
                return null;
            }
        }

        public List<PageLink> GetPageLinks(Chapter chapter, TextWriter logger)
        {
            try
            {
                var web = new HtmlWeb();
                var pageDoc = web.Load(BASE_URL + chapter.ChapterURL);
                var utcTimeNow = Helpers.GetUtcTimeNowDB();

                System.Threading.Thread.Sleep(1000 * r.Next(1, 5));

                if (web.StatusCode != HttpStatusCode.OK)
                {
                    throw new WebLoadException($"{SiteName.ToString()} scraper:{BASE_URL}+{chapter.ChapterURL} is incorrect - chapter is dirty");
                }

                var pageLinks = new List<PageLink>();
                var options = pageDoc.DocumentNode.SelectNodes("//*[@id='pageMenu']//option");
                var pageUrls = new Dictionary<int, String>();

                for (int j = 0; j < options.Count; j++)
                {
                    var option = options[j];
                    pageUrls.Add(int.Parse(option.InnerText.Trim()), option.Attributes["value"].Value.Trim());
                }

                // only download if there are no records or there is a mismatch with online and local records
                if (chapter.PageLinks == null || chapter.PageLinks.Count != options.Count)
                {
                    // download
                    foreach (var index in pageUrls.Keys)
                    {
                        // return nothing so state gets picked up next iteration
                        if (!Running)
                        {
                            return null;
                        }

                        pageUrls.TryGetValue(index, out string pageUrl);

                        if (pageUrl != null)
                        {
                            var doc = web.Load(BASE_URL + pageUrl);

                            System.Threading.Thread.Sleep(1000 * r.Next(1, 5));

                            if (web.StatusCode == HttpStatusCode.OK)
                            {
                                var imgUrl = doc.DocumentNode.SelectSingleNode("//*[@id='img']").Attributes["src"].Value.Trim();

                                if (imgUrl != null)
                                {
                                    var pageLink = new PageLink(index, imgUrl, pageUrl, chapter, utcTimeNow);
                                    pageLinks.Add(pageLink);
                                }
                            }
                            else
                            {
                                throw new WebLoadException($"{SiteName.ToString()} scraper:{BASE_URL}+{pageUrl} is incorrect - chapter is dirty");
                            }
                        }
                    }
                    return pageLinks;
                }
                else
                {
                    return chapter.PageLinks.ToList();
                }
            }
            catch (WebLoadException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                Helpers.PrintErrors(e, logger);
                logger.Write($"url={BASE_URL + chapter.ChapterURL}\n");
                logger.Flush();
                return null;
            }
        }
    }
}

