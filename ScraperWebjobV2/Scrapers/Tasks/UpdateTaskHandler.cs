﻿using ScraperWebjobV2.Db;
using ScraperWebjobV2.Misc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using ScraperWebjobV2.Models;
using System.Data.Entity;

namespace ScraperWebjobV2.Scrapers.Tasks
{
    class UpdateTaskHandler : AbstractTaskHandler
    {
        private TextWriter _logger;

        public UpdateTaskHandler(TextWriter logger)
        {
            _logger = logger;
        }

        internal override void CancelTask()
        {
            active = false;
        }

        internal override void HandleTask(ScraperTask task)
        {
            using (var context = new WebjobContext())
            {
                active = true;
                AssignedTask = task;
                Scraper = GetScraperInstance(AssignedTask);

                try
                {
                    var scraper = GetScraperInstance(AssignedTask);
                    var site = GetSite(context, AssignedTask, scraper);

                    ApplicationState.AcquireSiteTBLock();
                    ApplicationState.AcquireSiteDBLock(AssignedTask);

                    var dirtySourceCount = context.Sources.Where(s => s.SiteID == site.SiteID && s.Dirty).Count();
                    var runningSourceCount = context.Sources.Where(s => s.SiteID == site.SiteID && s.Running).Count();

                    if (dirtySourceCount > 0)
                    {
                        RemoveDirtySources(context, site);
                    }

                    if (runningSourceCount > 0)
                    {
                        // mark running sources as not running
                        DisableRunningSources(context, site);
                    }

                    var lastUpdated = site.LastUpdated;
                    // TODO need to add days to prevent checks everyday
                    // no need - task scheduler webjob runs every n(8) hours
                    if (lastUpdated == null || DateTime.SpecifyKind(DateTime.Parse(lastUpdated), DateTimeKind.Utc) < DateTime.Now)
                    {
                        // queue task
                        site.LastUpdated = DateTime.UtcNow.ToString(Helpers.GetUtcTimeNowDB());

                        context.SaveChanges();

                        ApplicationState.CreateInitializeTask(AssignedTask.SiteName);
                        var outputString = $"[{Helpers.GetTimeNowLog()}] {Scraper.SiteName.ToString()} update - adding {AssignedTask.SiteName.ToString()} {ScraperTask.ScraperTaskType.Initialize.ToString()} task\n";
                        _logger.Write(outputString);
                    }
                }
                finally
                {
                    ApplicationState.ReleaseSiteDBLock(AssignedTask);
                    ApplicationState.ReleaseSiteTBLock();

                    active = false;
                    ApplicationState.RemoveTask(this);
                }
            }
        }

        private void DisableRunningSources(WebjobContext context, Site site)
        {
            // turn off unused running sources
            var outputString = "";
            // load up all running sources
            var runningSources = context.Sources.Where(so => so.SiteID == site.SiteID && so.Running && !so.Dirty).ToList();
            // find all sources used by activetasks
            var taskHandlers = ApplicationState.GetTaskHandlers();
            // remove found sources from all running sources
            // n^2
            foreach (var taskHandler in taskHandlers)
            {
                var activeScraperTask = taskHandler.AssignedTask;

                var res = activeScraperTask.SiteName == AssignedTask.SiteName
                    && activeScraperTask.Type.Equals(ScraperTask.ScraperTaskType.DownloadDetails);

                if (res)
                {
                    var dbSources = (taskHandler as DownloadTaskHandler).DbSources;
                    if (dbSources != null)
                    {
                        foreach (var dbSource in dbSources)
                        {
                            runningSources.RemoveAll(so => so.SourceID == dbSource.SourceID);
                        }
                    }
                }
            }

            foreach (var source in runningSources)
            {
                source.Running = false;
            }

            outputString = $"[{Helpers.GetTimeNowLog()}] {Scraper.SiteName.ToString()} update - disabling {runningSources.Count()} running entries\n";
            _logger.Write(outputString);
            _logger.Flush();

            context.SaveChanges();
        }

        private void RemoveDirtySources(WebjobContext context, Site site)
        {
            var outputString = "";

            var dirtySources = context.Sources
                .Where(so => so.SiteID == site.SiteID && so.Dirty && !so.Running)
                .Include(so => so.Detail);;
            context.Sources.RemoveRange(dirtySources);

            outputString = $"[{Helpers.GetTimeNowLog()}] {Scraper.SiteName.ToString()} update - removing {dirtySources.Count()} dirty entries\n";
            _logger.Write(outputString);
            _logger.Flush();

            context.SaveChanges();
        }
    }
}
