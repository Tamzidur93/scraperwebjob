﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScraperWebjobV2.Scrapers.Tasks
{
    public class ScraperTask
    {

        public enum ScraperTaskType
        {
            Initialize, DownloadDetails, Update
        }

        // init = initial download (mangas and source links)
        // download = details, chapters, individual page links
        // update = generate tasks when sites are older than n days (n > 0)

        public ScraperTask()
        {
        }

        public ScraperTask(SiteName siteName, ScraperTaskType type)
        {
            SiteName = siteName;
            Type = type;
        }

        public ScraperTask(SiteName siteName, ScraperTaskType type, string parameters)
        {
            SiteName = siteName;
            Type = type;
            Parameters = parameters;
        }

        public SiteName SiteName { get; set; }
        public ScraperTaskType Type { get; set; }
        public string Parameters { get; set; } // csv (key: val)        

        public override bool Equals(object obj)
        {
            var item = obj as ScraperTask;

            if (item == null || GetType() != item.GetType())
            {
                return false;
            }

            return SiteName.Equals(item.SiteName)
                && Type.Equals(item.Type) 
                && (item.Parameters == Parameters || Parameters.ToLower().Equals(item.Parameters.ToLower())); // null parameters case
        }

        public override int GetHashCode()
        {
            return 0;
        }
    }
}
