﻿using ScraperWebjobV2.Db;
using ScraperWebjobV2.Misc;
using ScraperWebjobV2.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScraperWebjobV2.Scrapers.Tasks
{
    class InitializeTaskHandler : AbstractTaskHandler
    {
        private TextWriter _logger = null;

        public InitializeTaskHandler(TextWriter logger)
        {
            _logger = logger;
        }

        internal override void CancelTask()
        {
            active = false;
            if (Scraper != null)
            {
                Scraper.Running = false;
            }
        }

        // [Initialize] -> [insert sources & mangas] -> queue download tasks
        internal override void HandleTask(ScraperTask task)
        {
            using (var context = new WebjobContext())
            {
                try
                {
                    active = true;
                    AssignedTask = task;
                    Scraper = GetScraperInstance(AssignedTask);

                    // add or update site
                    var site = GetSite(context, AssignedTask, Scraper);
                    var sourceCount = -1;
                    var outputString = "";

                    var createMangaList = new List<Manga>();

                    var createSourceList = new List<Source>();
                    var updateSourceList = new List<Source>();
                    var deleteSourceList = new List<Source>();

                    List<Source> sources = null;

                    // [insert sources & mangas] -> get sources from scraper -> [filter mangas] -> insert mangas -> save changes -> 
                    //                              [map sources] -> [filter sources] -> add to database -> save changes
                    // get sources from scraper
                    try
                    {
                        sources = Scraper.GetSources(site, _logger);
                    }
                    catch (WebLoadException e)
                    {
                        Helpers.PrintErrors(e, _logger);
                    }

                    if (sources == null)
                    {
                        outputString = $"[{Helpers.GetTimeNowLog()}] {Scraper.SiteName.ToString()} initialize task failed: {Scraper.SiteName.ToString()} scraper returned null sources\n";
                        _logger.Write(outputString);
                        return;
                    }

                    // remove duplicate entries
                    for (int i = 0; i < sources.Count; i++)
                    {
                        var source = sources[i];

                        for (int j = 0; j < sources.Count; j++)
                        {
                            var res = sources[j];
                            if (res.Manga.Equals(source.Manga) && i != j)
                            {
                                sources.RemoveAt(j);
                                i--;
                                break;
                            }
                        }
                    }

                    try
                    {
                        ApplicationState.AcquireMangaTBLock();
                        ApplicationState.AcquireSiteDBLock(AssignedTask);

                        foreach (var source in sources)
                        {
                            createMangaList.Add(source.Manga);
                        }
                        createSourceList.AddRange(sources);

                        var dbMangas = context.Mangas.ToList();

                        // [filter mangas]
                        FilterManga(createMangaList, dbMangas);

                        // insert mangas
                        context.Mangas.AddRange(createMangaList);

                        // save changes
                        context.SaveChanges();

                        // have to reload
                        dbMangas = context.Mangas.ToList();

                        // [map sources]
                        MapSourcesToMangaToManga(createSourceList, dbMangas);

                        var dbSources = context.Sources
                            .Where(so => so.SiteID == site.SiteID)
                            .Include(so => so.Manga)
                            .Include(so => so.Site)
                            .ToList();

                        // [filter sources]
                        FilterSources(createSourceList, updateSourceList, deleteSourceList, dbSources);

                        // add to database
                        context.Sources.AddRange(createSourceList);

                        // save changes
                        context.SaveChanges();

                        sourceCount = context.Sources.Where(so => so.SiteID == site.SiteID).Count();
                    }
                    catch (Exception e)
                    {
                        Helpers.PrintErrors(e, _logger);
                    }
                    finally
                    {
                        ApplicationState.ReleaseSiteDBLock(AssignedTask);
                        ApplicationState.ReleaseMangaTBLock();
                    }

                    if (sourceCount != -1 && active)
                    {
                        int threadCount = sourceCount / THREAD_STRIDE;
                        if (sourceCount % THREAD_STRIDE != 0) threadCount++;

                        outputString = $"[{Helpers.GetTimeNowLog()}] {Scraper.SiteName.ToString()} initialize: started {threadCount} tasks\n";
                        _logger.Write(outputString);

                        // queue download tasks
                        for (int i = 0; i < threadCount; i++)
                        {
                            ApplicationState.CreateDownloadTask(Scraper.SiteName, $"{i}:{threadCount}");
                        }
                    }
                    else
                    {
                        outputString = $"[{Helpers.GetTimeNowLog()}] {Scraper.SiteName.ToString()} initialize task failed: sourceCount={sourceCount}, active={active}\n";
                        _logger.Write(outputString);
                    }

                    // log message
                    outputString = $"[{Helpers.GetTimeNowLog()}] {Scraper.SiteName.ToString()} initialize task completed\n";
                    outputString += $"Added {createMangaList.Count} Mangas\n";
                    outputString += $"Added {createSourceList.Count} Sources\n";
                    outputString += $"Updated {updateSourceList.Count} Sources\n";
                    outputString += $"Marked {deleteSourceList.Count} Sources for deletion\n";

                    _logger.Write(outputString);

                    try
                    {
                        ApplicationState.AcquireSiteTBLock();
                        site.LastUpdated = DateTime.UtcNow.ToString(Helpers.GetUtcTimeNowDB());
                        context.SaveChanges();
                    }
                    finally
                    {
                        ApplicationState.ReleaseSiteTBLock();
                    }
                }
                finally
                {
                    // clean up
                    active = false;
                    ApplicationState.RemoveTask(this);
                }
            }
        }

        // [FilterManga] -> remove local mangas from create list 
        private static void FilterManga(List<Manga> createMangaList, List<Manga> dbMangas)
        {
            foreach (var dbManga in dbMangas)
            {
                Manga newManga;

                if ((newManga = createMangaList.Find(m => m.Equals(dbManga))) != null)
                {
                    createMangaList.Remove(newManga);
                }
            }
        }

        // [MapSourcesToManga] -> update sourcelist with local mangas
        private void MapSourcesToMangaToManga(List<Source> createSourceList, List<Manga> dbMangas)
        {
            // cannot add references without objectid - framework creates new entries for all objects without id
            // find and replace newsource's manga with dbmanga to prevent duplicate entries in mangatb
            foreach (var dbManga in dbMangas)
            {
                Source newSource;

                if ((newSource = createSourceList.Find(so => so.Manga.Equals(dbManga))) != null)
                {
                    newSource.Manga = dbManga;
                }
            }
        }


        // [FilterSources] -> remove new sources that have same manga as local source -> add new source to delete if local source isnt found in new list
        private static void FilterSources(List<Source> createSourceList, List<Source> updateSourceList, List<Source> deleteSourceList, List<Source> dbSources)
        {
            foreach (var dbSource in dbSources)
            {
                // equivalency on manga names
                var newSource = createSourceList.Find(so => so.Manga.Equals(dbSource.Manga));

                // try to find a source mapped to same manga as existing source in new source list
                if (newSource != null)
                {
                    // if existing source is being modified by another thread or is marked for deletion remove from creation list
                    // newSource should never be null
                    if ((!dbSource.Running && !dbSource.Dirty))
                    {
                        // if new source doesnt match existing one 
                        if (!dbSource.Equals(newSource))
                        {
                            dbSource.SourceURL = newSource.SourceURL;
                            updateSourceList.Add(dbSource);
                        }
                    }
                    createSourceList.Remove(newSource);
                }
                // if no match is found mark source for deletion
                else
                {
                    deleteSourceList.Add(dbSource);
                    dbSource.Dirty = true;
                }
            }
        }
    }
}
