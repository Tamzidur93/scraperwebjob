﻿using ScraperWebjobV2.Db;
using ScraperWebjobV2.Misc;
using ScraperWebjobV2.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScraperWebjobV2.Scrapers.Tasks
{
    internal abstract class AbstractTaskHandler
    {
        internal ScraperTask AssignedTask { get; set; }
        internal IScraper Scraper { get; set; }
        internal bool active = true;

#if DEBUG
        internal const int THREAD_STRIDE = 2000;
#else
        internal const int THREAD_STRIDE = 500;
#endif    
        
        internal abstract void HandleTask(ScraperTask task);
        internal abstract void CancelTask();

        internal IScraper GetScraperInstance(ScraperTask scraperTask)
        {
            IScraper scraper = null;
            switch (scraperTask.SiteName)
            {
                case SiteName.MangaReader:
                    scraper = new MangaReader();
                    break;
                case SiteName.MangaPanda:
                    scraper = new MangaPanda();
                    break;
                case SiteName.MangaFox:
                    scraper = new MangaFox();
                    break;
                default:
                    throw new NotImplementedException($"{scraperTask.SiteName.ToString()} does not have a GetScraperInstance implementation");
            }
            return scraper;
        }

        internal Site GetSite(WebjobContext context, ScraperTask scraperTask, IScraper scraper)
        {
            try
            {
                ApplicationState.AcquireSiteTBLock();

                var exists = context.Sites.Where(s => s.Name.Equals(scraperTask.SiteName.ToString())).Count() > 0;

                if (!exists)
                {
                    var utcTimeNow = Helpers.GetUtcTimeNowDB();

                    context.Sites.Add(new Site(name: scraper.SiteName.ToString(), siteURL: scraper.BaseURL, lastModified: utcTimeNow));
                    context.SaveChanges();
                }

                return context.Sites.Where(s => s.Name == scraper.SiteName.ToString()).First();
            }
            finally
            {
                ApplicationState.ReleaseSiteTBLock();
            }
        }

    }
}
