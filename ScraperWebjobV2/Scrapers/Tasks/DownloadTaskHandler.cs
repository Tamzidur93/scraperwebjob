﻿using ScraperWebjobV2.Db;
using ScraperWebjobV2.Misc;
using ScraperWebjobV2.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScraperWebjobV2.Scrapers.Tasks
{
    class DownloadTaskHandler : AbstractTaskHandler
    {
        private TextWriter _logger = null;
        public List<Source> DbSources { get; set; }

        public DownloadTaskHandler(TextWriter logger)
        {
            _logger = logger;
        }

        internal override void CancelTask()
        {
            active = false;
            if (Scraper != null)
            {
                Scraper.Running = false;
            }
        }

        internal override void HandleTask(ScraperTask task)
        {
            using (var context = new WebjobContext())
            {
                try
                {
                    active = true;
                    AssignedTask = task;
                    Scraper = GetScraperInstance(AssignedTask);

                    var threadNumber = -1;
                    var threadCount = -1;
                    var startIndex = -1;
                    var endIndex = -1;

                    var site = GetSite(context, AssignedTask, Scraper); // add or update sites                    

                    var sourceIndex = 0;
                    var outputString = "";

                    // AIMD
                    var updateFound = false;

                    try
                    {
                        var parameters = AssignedTask.Parameters.Split(':');
                        threadNumber = int.Parse(parameters[0]);
                        threadCount = int.Parse(parameters[1]);
                    }
                    catch (Exception e)
                    {
                        _logger.WriteLine($"[{Helpers.GetTimeNowLog()}] {AssignedTask.SiteName.ToString()} [{threadNumber + 1}/{threadCount}] download - params={AssignedTask.Parameters} - parameter format incorrect");
                        Helpers.PrintErrors(e, _logger);
                        return;
                    }

                    startIndex = threadNumber * THREAD_STRIDE;
#if DEBUG
                    endIndex = Math.Min(context.Sources.Where(so => so.SiteID == site.SiteID).Count(), startIndex + 3);
#else
                    endIndex = Math.Min(context.Sources.Where(so => so.SiteID == site.SiteID).Count(), startIndex + THREAD_STRIDE);
#endif

                    DbSources = GetSources(
                        context: context,
                        site: site,
                        startIndex: startIndex,
                        endIndex: endIndex,
                        task: AssignedTask
                        );

                    if (DbSources == null)
                    {
                        _logger.WriteLine($"[{Helpers.GetTimeNowLog()}] {task.SiteName.ToString()} [{threadNumber + 1}/{threadCount}] download - failed: failed to acquire sources");
                        return;
                    }

                    foreach (var dbSource in DbSources)
                    {
                        _logger.WriteLine($"[{Helpers.GetTimeNowLog()}] {AssignedTask.SiteName.ToString()} [{threadNumber + 1}/{threadCount}] download - started: manga={dbSource.Manga.Name}, source={dbSource.SourceID}");

                        if (!active)
                        {
                            break;
                        }

                        var lastUpdated = DateTime.SpecifyKind(DateTime.Parse(dbSource.LastModified), DateTimeKind.Utc);

                        if (!InsertDetails(context, dbSource))
                        {
                            continue;
                        }

                        if (!InsertChapters(context, threadCount, threadNumber, ref sourceIndex, ref updateFound, dbSource))
                        {
                            continue;
                        }

                        // have to load again
                        var dbChapters = context.Chapters
                            .Where(c => c.SourceID == dbSource.SourceID)
                            .Include(c => c.PageLinks)
                            .ToList();

                        foreach (var dbChapter in dbChapters)
                        {
                            if (!active)
                            {
                                break;
                            }

                            if (!InsertPageLinks(context, sourceIndex, threadCount, threadNumber, ref updateFound, dbSource, dbChapter))
                            {
                                continue;
                            }
                        }

                        // calculate s[x+1] given s[x]
                        dbSource.UpdateFreqSequence = CalculateUpdateFreqSequence(
                            updateFreqSequence: dbSource.UpdateFreqSequence,
                            lastUpdated: lastUpdated,
                            lastModified: DateTime.SpecifyKind(DateTime.Parse(dbSource.LastModified), DateTimeKind.Utc),
                            updateFound: updateFound
                            );

                        dbSource.Running = false;
                        context.SaveChanges();

                        sourceIndex++;
                    }
                    // output strings
                    outputString = $"[{Helpers.GetTimeNowLog()}] {Scraper.SiteName.ToString()} [{threadNumber + 1}/{threadCount}] download - task completed: threadNumber={threadNumber + 1}/{threadCount}, index={sourceIndex}/{DbSources.Count}\n";
                    _logger.Write(outputString);
                    _logger.Flush();

                    // turn off all running sources, 
                    foreach (var dbSource in DbSources)
                    {
                        if (dbSource.Running)
                        {
                            // if downloaddetails is force cancelled - check unchecked sources in 1 day and adjust avging system
                            if (dbSource.UpdateFreqSequence != null)
                            {
                                var values = dbSource.UpdateFreqSequence.Split(',');
                                dbSource.UpdateFreqSequence = Helpers.AppendUpdateFreqSequence(values, 1);
                            }
                            else
                            {
                                dbSource.UpdateFreqSequence = "1";
                            }

                            dbSource.Running = false;
                        }
                    }

                    context.SaveChanges();

                    try
                    {
                        ApplicationState.AcquireSiteTBLock();
                        site.LastUpdated = DateTime.UtcNow.ToString(Helpers.GetUtcTimeNowDB());
                        context.SaveChanges();
                    }
                    finally
                    {
                        ApplicationState.ReleaseSiteTBLock();
                    }
                }
                finally
                {
                    // clean up
                    active = false;
                    ApplicationState.RemoveTask(this);
                }
            }
        }

        internal List<Source> GetSources(
            WebjobContext context,
            Site site,
            int startIndex,
            int endIndex,
            ScraperTask task
            )
        {

            List<Source> res = null;

            // [getsources]-> acquire lock -> load db for sources with site -> mark sources as running -> release lock
            try
            {
                // acquire lock
                ApplicationState.AcquireSiteDBLock(task);

                // load db sources with site
                // try to always have THREAD_STRIDE per thread
                res = context.Sources
                    //.Where(so => so.SiteID.Equals(site.SiteID) && !so.Dirty && !so.Running)
                    .Where(so => so.SiteID.Equals(site.SiteID))
                    .OrderBy(so => so.SourceID)
                    .Skip(startIndex)
                    .Take(endIndex - startIndex)
                    .Include(so => so.Manga)
                    .Include(so => so.Site)
                    .Include(so => so.Chapters)
                    .ToList();

                for (int i = 0; i < res.Count; i++)
                {
                    if (res[i].Running || res[i].Dirty)
                    {
                        res.RemoveAt(i);
                        i--;
                    }
                    else
                    {
                        res[i].Running = true;
                    }
                }

                //// mark sources as running
                //foreach (var dbSource in res)
                //{

                //    dbSource.Running = true;
                //}

                context.SaveChanges();
            }
            catch (Exception e)
            {
                Helpers.PrintErrors(e, _logger);
                return res;
            }
            finally
            {
                // release lock
                ApplicationState.ReleaseSiteDBLock(task);
            }

            return res;
        }

        private bool InsertPageLinks(
            WebjobContext context,
            int sourceIndex,
            int threadCount,
            int threadNumber,
            ref bool updateFound,
            Source dbSource,
            Chapter dbChapter)
        {
            // [insert pagelinks] -> download links with scraper -> [FilterPageLinks] -> insert page links -> save changes
            List<PageLink> createPageList = null;
            var updatePageList = new List<PageLink>();
            var deletePageList = new List<PageLink>();
            var outputString = "";

            try
            {
                createPageList = Scraper.GetPageLinks(dbChapter, _logger);
            }
            catch (WebLoadException e)
            {
                Helpers.PrintErrors(e, _logger);
                dbChapter.Dirty = true;
                context.SaveChanges();
            }

            if (createPageList == null)
            {
                outputString = $"[{Helpers.GetTimeNowLog()}] - {Scraper.SiteName.ToString()} [{threadNumber + 1}/{threadCount}] download - task failed: scraper={Scraper.SiteName.ToString()}, manga={dbSource.Manga.ToString()}, dbSource={dbSource.SourceID}, dbChapter={dbChapter.ChapterID}\n";
                outputString += $"cause={Scraper.SiteName.ToString()} scraper returned null pageLinkList\n";
                _logger.Write(outputString);
                return false;
            }

            var dbPageLinks = context.PageLinks
                .Where(p => p.ChapterID == dbChapter.ChapterID)
                .Include(p => p.Chapter)
                .ToList();

            // filter pagelinks
            FilterPageLinks(createPageList, updatePageList, deletePageList, dbPageLinks);

            if (createPageList.Count > 0)
            {
                updateFound = true;
            }

            // insert page links
            context.PageLinks.AddRange(createPageList);
            context.PageLinks.RemoveRange(deletePageList);

            // update source
            dbSource.LastModified = Helpers.GetUtcTimeNowDB();

            outputString = $"[{Helpers.GetTimeNowLog()}] {Scraper.SiteName.ToString()} [{threadNumber + 1}/{threadCount}] download - InsertPageLinks: manga={dbSource.Manga.Name}, source={dbSource.SourceID}, chapter={dbChapter.ChapterID}, progress={sourceIndex}/{DbSources.Count}\n";
            outputString += $"added {createPageList.Count} pages, updated {updatePageList.Count} pages, deleted {deletePageList.Count} pages\n";
            _logger.Write(outputString);
            _logger.Flush();

            // save changes
            context.SaveChanges();

            return true;
        }

        private bool InsertChapters(
            WebjobContext context,
            int threadCount,
            int threadNumber,
            ref int sourceIndex,
            ref bool updateFound,
            Source dbSource)
        {
            // [insert chapters] -> scrape chapters with scraper -> [FilterChapters] -> insert chapters -> save changes
            // scrape chapters with scraper 
            List<Chapter> createChapterList = null;
            var updateChapterList = new List<Chapter>();
            var deleteChapterList = new List<Chapter>();
            var outputString = "";

            try
            {
                createChapterList = Scraper.GetChapters(dbSource, _logger);
            }
            catch (WebLoadException e)
            {
                dbSource.Dirty = true;
                context.SaveChanges();
                Helpers.PrintErrors(e, _logger);
            }

            if (createChapterList == null)
            {
                outputString = $"[{Helpers.GetTimeNowLog()}] {Scraper.SiteName.ToString()} [{threadNumber + 1}/{threadCount}] download - task failed: scraper={Scraper.SiteName.ToString()}, manga={dbSource.Manga}, dbSource={dbSource.SourceID}\n";
                outputString += $"cause={Scraper.SiteName.ToString()} scraper returned null createChapterList\n";
                _logger.Write(outputString);
                return false;
            }

            // [CalculateSkipConditions]
            if (CalculateSkipConditions(dbSource, createChapterList))
            {
                var lastModified = DateTime.SpecifyKind(DateTime.Parse(dbSource.LastModified), DateTimeKind.Utc);
                var updateFreqs = dbSource.UpdateFreqSequence.Split(',');
                var updateAge = int.Parse(updateFreqs[updateFreqs.Length - 1]);

                var last = lastModified.ToString(Helpers.LogTimeFormat);
                var next = lastModified.AddDays(updateAge).ToString(Helpers.LogTimeFormat);

                outputString = $"[{Helpers.GetTimeNowLog()}] {Scraper.SiteName.ToString()} [{threadNumber + 1}/{threadCount}] download - skipped:\n";
                outputString += $"manga={dbSource.Manga.Name}, last/next={last}/{next}, local/online={dbSource.Chapters.Count}/{createChapterList.Count}\n";
                _logger.Write(outputString);

                //dbSource.LastModified = Helpers.GetUtcTimeNowDB();
                dbSource.Running = false;
                context.SaveChanges();
                sourceIndex++;
                return false;
            }

            var dbChapters1 = context.Chapters
                .Where(c => c.SourceID == dbSource.SourceID)
                .Include(c => c.Source)
                .Include(c => c.Source.Manga)
                .Include(c => c.Source.Site)
                .ToList();

            // filter chapters
            FilterChapters(createChapterList, updateChapterList, deleteChapterList, dbChapters1);

            // used for dbSource.UpdateFreqSequence
            if (createChapterList.Count > 0)
            {
                updateFound = true;
            }

            // insert chapters
            context.Chapters.AddRange(createChapterList);
            context.Chapters.RemoveRange(deleteChapterList);

            outputString = $"[{Helpers.GetTimeNowLog()}] {Scraper.SiteName.ToString()} [{threadNumber + 1}/{threadCount}] download - InsertChapters: manga={dbSource.Manga.Name}, source={dbSource.SourceID}, progress={sourceIndex}/{DbSources.Count}\n";
            outputString += $"added {createChapterList.Count} chapters, updated {updateChapterList.Count} chapters, deleted {deleteChapterList.Count} chapters \n";
            _logger.Write(outputString);
            _logger.Flush();

            // save changes to db
            context.SaveChanges();

            return true;
        }

        private bool InsertDetails(WebjobContext context, Source dbSource)
        {
            // [insertdetails] -> scrape details with scraper -> [FilterDetail] -> save changes                        
            // scraper details with scraper
            Detail detail = null;

            try
            {
                detail = Scraper.GetDetails(dbSource, _logger);
            }
            catch (WebLoadException e)
            {
                dbSource.Dirty = true;
                context.SaveChanges();
                Helpers.PrintErrors(e, _logger);
            }

            if (detail == null)
            {
                return false;
            }

            // check if detail exists in database for sourceID
            var dbDetail = context.Details
                .Where(d => d.DetailID == dbSource.SourceID)
                .Include(d => d.Source.Manga)
                .Include(d => d.Source.Site)
                .FirstOrDefault();

            FilterDetails(context, detail, dbDetail);


            // save changes
            context.SaveChanges();

            return true;
        }

        // returns what the next element of the string should be
        internal static string CalculateUpdateFreqSequence(String updateFreqSequence, DateTime lastUpdated, DateTime lastModified, bool updateFound)
        {
            var res = "";

            // AIMD
            if (updateFreqSequence == null)
            {
                res = "1";
            }
            else if (updateFound)
            {
                var sum = 0d;
                var avg = 0;
                var values = updateFreqSequence.Split(',');
                var next = 1;

                for (int i = 0; i < values.Length; i++)
                {
                    sum += int.Parse(values[i]);
                }

                // lastModified should be newer
                var updateFreq = (int)(lastModified - lastUpdated).TotalDays;
                avg = (int)Math.Round(sum / values.Length);

                if (avg > updateFreq)
                {
                    next = Math.Max(1, updateFreq);
                    next = Math.Min(14, next);
                    res = Helpers.AppendUpdateFreqSequence(values, next);
                }
                else
                {
                    // lower bound 1
                    next = Math.Max(1, avg);
                    // max bound 14
                    next = Math.Min(14, next);
                    res = Helpers.AppendUpdateFreqSequence(values, next);
                }
            }
            else
            {
                var values = updateFreqSequence.Split(',');
                // max bound 14
                var next = Math.Min(int.Parse(values[values.Length - 1]) + 3, 14);

                res = Helpers.AppendUpdateFreqSequence(values, next); ;
            }

            return res;
        }

        private static void FilterPageLinks(List<PageLink> createPageList, List<PageLink> updatePageList, List<PageLink> deletePageList, List<PageLink> dbPageLinks)
        {
            foreach (var dbPageLink in dbPageLinks)
            {
                var pageLink = createPageList.Find(p => p.PageIndex == dbPageLink.PageIndex);

                if (pageLink != null)
                {
                    dbPageLink.ImageURL = pageLink.ImageURL;
                    dbPageLink.PageURL = pageLink.PageURL;
                    dbPageLink.PageIndex = pageLink.PageIndex;

                    updatePageList.Add(dbPageLink);

                    createPageList.Remove(pageLink);
                }
                else
                {
                    deletePageList.Add(dbPageLink);
                }
            }
        }

        private static void FilterChapters(List<Chapter> createChapterList, List<Chapter> updateChapterList, List<Chapter> deleteChapterList, List<Chapter> dbChapters)
        {
            foreach (var dbChapter in dbChapters)
            {
                // find a newSource that has same index as dbSource
                var chapter = createChapterList.Find(c => c.ChapterIndex == dbChapter.ChapterIndex);

                if (chapter != null)
                {
                    // remove newSource from create list if found
                    createChapterList.Remove(chapter);

                    // update if newSource is different to dbSource
                    if (!chapter.Equals(dbChapter))
                    {
                        dbChapter.ChapterIndex = chapter.ChapterIndex;
                        dbChapter.ChapterName = chapter.ChapterName;
                        dbChapter.ChapterURL = chapter.ChapterURL;
                        dbChapter.LastModified = chapter.LastModified;

                        updateChapterList.Add(dbChapter);
                    }
                }
                else
                {
                    // if find() returns null, chapter has been deleted from site
                    deleteChapterList.Add(dbChapter);
                }
            }
        }

        private static void FilterDetails(WebjobContext context, Detail newDetail, Detail oldDetail)
        {
            // if exists, update to new
            if (oldDetail != null)
            {
                oldDetail.AlternateNames = newDetail.AlternateNames;
                oldDetail.Artists = newDetail.Artists;
                oldDetail.Authors = newDetail.Authors;
                oldDetail.Genre = newDetail.Genre;
                oldDetail.ImageURL = newDetail.ImageURL;
                oldDetail.Source = newDetail.Source;
                oldDetail.Status = newDetail.Status;
                oldDetail.Summary = newDetail.Summary;
                oldDetail.YearOfRelease = newDetail.YearOfRelease;
            }
            else
            {
                // if doesnt exist, add new
                context.Details.Add(newDetail);
            }
        }

        // [CalculateSkipCondition] -> assume no updates required -> check conditions to determine if required -> return result
        // true -> skip, false -> dont skip
        private static bool CalculateSkipConditions(Source dbSource, List<Chapter> createChapterList)
        {
            var res = true;

            if (dbSource.UpdateFreqSequence == null)
            {
                dbSource.UpdateFreqSequence = "1";
            }

            var updateFreqs = dbSource.UpdateFreqSequence.Split(',');
            var updateAge = int.Parse(updateFreqs[updateFreqs.Length - 1]);

            // condition: ( !(source has no chapters) && !(lastUpdate is older than a day) && !(local and online chapterCounts dont match) && !(any chapter has null or 0 pagelinks))
            // will pass if (onlinelinks > pagelinks > 0) in case chapter download gets canned early
            var lastModified = DateTime.SpecifyKind(DateTime.Parse(dbSource.LastModified), DateTimeKind.Utc);
            if (dbSource.Chapters == null || lastModified.AddDays(updateAge) < DateTime.Now || dbSource.Chapters.Count != createChapterList.Count)
            {
                res = false;
            }

            if (dbSource.Chapters != null && !res)
            {
                foreach (var chapter in dbSource.Chapters)
                {
                    if (chapter.PageLinks == null || chapter.PageLinks.Count == 0 || chapter.Dirty)
                    {
                        res = false;
                        break;
                    }
                }
            }

            return res;
        }
    }
}
