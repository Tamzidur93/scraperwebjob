﻿using ScraperWebjobV2.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScraperWebjobV2.Scrapers
{
    interface IScraper
    {
        bool Running { get; set; }
        SiteName SiteName { get; }
        string BaseURL { get; }   
        List<Source> GetSources(Site site, TextWriter logger);
        Detail GetDetails(Source source, TextWriter logger);
        List<Chapter> GetChapters(Source source, TextWriter logger);
        List<PageLink> GetPageLinks(Chapter chapter, TextWriter logger);
    }

    class WebLoadException : Exception
    {
        public WebLoadException(string message) : base(message)
        {
        }
    }
}
