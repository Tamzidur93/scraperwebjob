﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ScraperWebjobV2.Models;

namespace ScraperWebjobV2.Scrapers
{
    class MangaFox : IScraper
    {
        private bool running = false;
        private const SiteName SITE_NAME = SiteName.MangaFox;
        private const string BASE_URL = "www.mangafox.com";

        public bool Running { get => running; set => running = value; }
        public SiteName SiteName{ get => SITE_NAME; }
        public string BaseURL { get => BASE_URL; }

        public List<Source> GetSources(Site site, TextWriter logger)
        {
            throw new NotImplementedException();
        }

        public Detail GetDetails(Source source, TextWriter logger)
        {
            throw new NotImplementedException();
        }

        public List<Chapter> GetChapters(Source source, TextWriter logger)
        {
            throw new NotImplementedException();
        }

        public List<PageLink> GetPageLinks(Chapter chapter, TextWriter logger)
        {
            throw new NotImplementedException();
        }
    }
}
