﻿using Microsoft.Azure.WebJobs;
using Microsoft.WindowsAzure.Storage.Queue;
using Newtonsoft.Json;
using ScraperWebjobV2.Scrapers;
using ScraperWebjobV2.Scrapers.Tasks;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static ScraperWebjobV2.Scrapers.Tasks.ScraperTask;

namespace ScraperWebjobV2.Misc
{
    public static class ApplicationState
    {
        private static JobHost host;

        private static List<AbstractTaskHandler> TaskHandlers = new List<AbstractTaskHandler>();

        private static Semaphore mangaReaderDBSemaphore = new Semaphore(1, 1);
        private static Semaphore mangaPandaDBSemaphore = new Semaphore(1, 1);
        private static Semaphore mangaFoxDBSemaphore = new Semaphore(1, 1);

        private static Semaphore mangaTBSemaphore = new Semaphore(1, 1);
        private static Semaphore siteTBSemaphore = new Semaphore(1, 1);

        internal static void StartScraper()
        {
            var storageConnectionString = ConfigurationManager.AppSettings.Get("StorageConnectionString");
            var config = new JobHostConfiguration(storageConnectionString);
            config.Queues.BatchSize = 32;
            host = new JobHost(config);

            //var configuration = new Migrations.Configuration();
            //var migrator = new DbMigrator(configuration);
            //migrator.Update();

            //CancellationTokenSource cts = new CancellationTokenSource(TimeSpan.FromMinutes(2));
            //var cancellationToken = cts.Token;

            var cancellationToken = new WebJobsShutdownWatcher().Token;

            cancellationToken.Register(StopScraper);

#if DEBUG
            var timer = new Timer((e) => GenerateTasks(), null, TimeSpan.Zero, TimeSpan.FromHours(4));
#endif

            Console.WriteLine($"[{Helpers.GetTimeNowLog()}] Scraper jobhost started");

            host.StartAsync();
        }

        internal static void StopScraper()
        {
            host.StopAsync();

            foreach (var taskHandlers in TaskHandlers)
            {
                taskHandlers.CancelTask();
            }
            
            Console.WriteLine($"[{Helpers.GetTimeNowLog()}] Scraper jobhost stopped");
        }

        // TODO timeouts
        internal static void AcquireSiteTBLock()
        {
            siteTBSemaphore.WaitOne();
        }

        // TODO timeouts
        internal static void AcquireMangaTBLock()
        {
            mangaTBSemaphore.WaitOne();
        }

        // TODO timeouts
        internal static void AcquireSiteDBLock(ScraperTask scraperTask)
        {
            switch (scraperTask.SiteName)
            {
                case SiteName.MangaReader:
                    mangaReaderDBSemaphore.WaitOne();
                    break;
                case SiteName.MangaPanda:
                    mangaPandaDBSemaphore.WaitOne();
                    break;
                case SiteName.MangaFox:
                    mangaPandaDBSemaphore.WaitOne();
                    break;
                default:
                    throw new NotImplementedException($"{scraperTask.SiteName.ToString()} does not have a AcquireSiteDBLock implementation");
            }
        }

        internal static void ReleaseSiteTBLock()
        {
            siteTBSemaphore.Release();
        }

        internal static void ReleaseMangaTBLock()
        {
            mangaTBSemaphore.Release();
        }

        internal static void ReleaseSiteDBLock(ScraperTask scraperTask)
        {
            switch (scraperTask.SiteName)
            {
                case SiteName.MangaReader:
                    mangaReaderDBSemaphore.Release();
                    break;
                case SiteName.MangaPanda:
                    mangaPandaDBSemaphore.Release();
                    break;
                case SiteName.MangaFox:
                    mangaPandaDBSemaphore.Release();
                    break;
                default:
                    throw new NotImplementedException($"{scraperTask.SiteName.ToString()} does not have a ReleaseSiteDBLock implementation");
            }
        }

        internal static int RunningTaskHandlers()
        {
            return TaskHandlers.Count;
        }

        internal static void GenerateTasks()
        {
            CreateUpdateTask(SiteName.MangaReader);
            CreateUpdateTask(SiteName.MangaPanda);
        }

        internal static void CreateTask(ScraperTask task)
        {
            var taskMessage = new CloudQueueMessage(JsonConvert.SerializeObject(task));
            var scraperQueue = Helpers.GetScraperQueueInstance();

            scraperQueue.AddMessageAsync(taskMessage);
        }

        internal static void CreateDelayedTask(ScraperTask task, TimeSpan timeSpan)
        {
            var taskMessage = new CloudQueueMessage(JsonConvert.SerializeObject(task));
            var scraperQueue = Helpers.GetScraperQueueInstance();

            scraperQueue.AddMessageAsync(taskMessage, null, timeSpan, null, null);
        }

        internal static void CreateUpdateTask(SiteName site)
        {
            var updateTask = new ScraperTask(siteName: site, type: ScraperTaskType.Update);
            var updateTaskMessage = new CloudQueueMessage(JsonConvert.SerializeObject(updateTask));
            var scraperQueue = Helpers.GetScraperQueueInstance();

            scraperQueue.AddMessageAsync(updateTaskMessage);
        }

        internal static void CreateDownloadTask(SiteName siteName, string parameters)
        {
            var downloadTask = new ScraperTask(siteName, ScraperTaskType.DownloadDetails, parameters);
            var downloadTaskMessage = new CloudQueueMessage(JsonConvert.SerializeObject(downloadTask));
            var scraperQueue = Helpers.GetScraperQueueInstance();

            scraperQueue.AddMessageAsync(downloadTaskMessage);
        }

        internal static void CreateInitializeTask(SiteName siteName)
        {
            var createTask = new ScraperTask(siteName, ScraperTaskType.Initialize);
            var createTaskMessage = new CloudQueueMessage(JsonConvert.SerializeObject(createTask));
            var scraperQueue = Helpers.GetScraperQueueInstance();

            scraperQueue.AddMessageAsync(createTaskMessage);
        }

        internal static void AddTask(AbstractTaskHandler taskController)
        {
            lock (TaskHandlers)
            {
                TaskHandlers.Add(taskController);
            }
        }

        internal static void RemoveTask(AbstractTaskHandler taskController)
        {
            lock (TaskHandlers)
            {
                TaskHandlers.Remove(taskController);
            }            
        }

        internal static List<AbstractTaskHandler> GetTaskHandlers()
        {
            return TaskHandlers;
        }
    }
}
