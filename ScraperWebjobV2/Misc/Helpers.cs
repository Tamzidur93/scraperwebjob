﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using ScraperWebjobV2.Db;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScraperWebjobV2.Misc
{
    static class Helpers
    {
        internal static string DatabaseTimeFormat { get; } = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'";
        internal static string LogTimeFormat { get; } = "yy'-'MM'-'dd'T'HH':'mm";

        internal static string GetUtcTimeNowDB()
        {
            var utcTimeNow = DateTime.Now.ToUniversalTime().ToString(DatabaseTimeFormat);
            return utcTimeNow;
        }

        internal static string GetTimeNowLog()
        {
            var utcTimeNow = DateTime.Now.ToUniversalTime().ToString(LogTimeFormat);
            return utcTimeNow;
        }

        internal static void ClearDatabase(WebjobContext context)
        {
            context.Mangas.RemoveRange(context.Mangas);
            context.Sites.RemoveRange(context.Sites);
            context.Sources.RemoveRange(context.Sources);
            context.Details.RemoveRange(context.Details);
            context.Chapters.RemoveRange(context.Chapters);
            context.PageLinks.RemoveRange(context.PageLinks);

            context.SaveChanges();
        }

        internal static void PrintErrors(Exception e)
        {
            Console.Error.WriteLine(e.StackTrace);
            while (e != null)
            {
                Console.Error.WriteLine(e.Message);
                e = e.InnerException;
            }
        }

        internal static void PrintErrors(Exception e, TextWriter logger)
        {
            logger.WriteLine(e.StackTrace);
            while (e != null)
            {
                logger.WriteLine(e.Message);
                e = e.InnerException;
            }
        }

        internal static void ClearScraperQueue()
        {
            var cloudQueue = GetScraperQueueInstance();
            cloudQueue.Clear();   
        }

        internal static CloudQueue GetScraperQueueInstance()
        {
            var connectionString = ConfigurationManager.AppSettings.Get("StorageConnectionString");

            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connectionString);
            CloudQueueClient queueClient = storageAccount.CreateCloudQueueClient();
            CloudQueue scraperQueue = queueClient.GetQueueReference("scraperqueue");

            scraperQueue.CreateIfNotExists();

            return scraperQueue;
        }

        internal static string AppendUpdateFreqSequence(string[] values, int next)
        {
            var output = "";
            var sequenceLimit = 4;

            for (int i = (values.Length == sequenceLimit) ? 1 : 0; i < values.Length; i++)
            {
                output += values[i] + ",";
            }

            output += next;
            return output;
        }

    }
}
