﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScraperWebjob.scrapers.tasks
{
    public class ScraperTask
    {

        public enum ScraperTaskType
        {
            Initialize, DownloadDetails, Update, Clean
        }

        // init = initial download (mangas and source links)
        // download = chapters, details and individual page links
        // check = mark entries in database older than 4 days as dirty -> queue refresh task
        // update = check update page
        // refresh = check and fix dirty entries

        public ScraperTask()
        {

        }

        public ScraperTask(SiteName site, ScraperTaskType type, int taskIndex)
        {
            SiteName = site;
            Type = type;
            TaskIndex = taskIndex;
        }

        public ScraperTask(SiteName siteName, ScraperTaskType type, int taskIndex, string parameters)
        {
            SiteName = siteName;
            Type = type;
            Parameters = parameters;
            TaskIndex = taskIndex;
        }

        public int TaskIndex { get; set; }
        public SiteName SiteName { get; set; }
        public ScraperTaskType Type { get; set; }
        public string Parameters { get; set; } // csv (key: val)        

        public override bool Equals(object obj)
        {
            

            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            var item = obj as ScraperTask;

            return SiteName.Equals(item.SiteName)
                && TaskIndex == item.TaskIndex
                && Type.Equals(item.Type) 
                && (item.Parameters == Parameters || Parameters.ToLower().Equals(item.Parameters.ToLower())); // null case
        }

        public override int GetHashCode()
        {
            return 0;
        }
    }
}
