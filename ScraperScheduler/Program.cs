﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ScraperWebjob.helpers;
using ScraperWebjob.scrapers;
using System.Threading;

namespace ScraperScheduler
{
    class Program
    {
        static void Main(string[] args)
        {
            var queue = Helpers.GetScraperQueueInstance();
            GenerateTasks();
        }

        private static void GenerateTasks()
        {
            ApplicationState.CreateUpdateTask(SiteName.MangaReader);
            ApplicationState.CreateUpdateTask(SiteName.MangaPanda);
        }
    }
}
