﻿using Microsoft.WindowsAzure.Storage.Queue;
using Newtonsoft.Json;
using ScraperWebjob.scrapers;
using ScraperWebjob.scrapers.tasks;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static ScraperWebjob.scrapers.tasks.ScraperTask;

namespace ScraperWebjob.helpers
{
    public static class ApplicationState
    {
        private static int mangaReaderTaskIndex = 0;

        public static void CreateUpdateTask(SiteName site)
        {
            var updateTask = new ScraperTask(site: site, type: ScraperTaskType.Update, taskIndex: mangaReaderTaskIndex);
            mangaReaderTaskIndex = (mangaReaderTaskIndex + 1) % 1024;
            var updateTaskMessage = new CloudQueueMessage(JsonConvert.SerializeObject(updateTask));
            var scraperQueue = Helpers.GetScraperQueueInstance();

            scraperQueue.AddMessage(updateTaskMessage);
        }
    }
}
