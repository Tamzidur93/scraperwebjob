﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScraperWebjob.helpers
{
    static class Helpers
    {
        public static void PrintErrors(Exception e)
        {
            Console.WriteLine(e.StackTrace);
            while (e != null)
            {
                Console.WriteLine(e.Message);
                e = e.InnerException;
            }
        }

        public static CloudQueue GetScraperQueueInstance()
        {
            var connectionString = ConfigurationManager.AppSettings.Get("StorageConnectionString");

            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connectionString);
            CloudQueueClient queueClient = storageAccount.CreateCloudQueueClient();
            CloudQueue scraperQueue = queueClient.GetQueueReference("scraperqueue");

            scraperQueue.CreateIfNotExists();

            return scraperQueue;
        }

        public static CloudQueue GetUpdateQueueInstance()
        {
            var connectionString = ConfigurationManager.AppSettings.Get("StorageConnectionString");

            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connectionString);
            CloudQueueClient queueClient = storageAccount.CreateCloudQueueClient();
            CloudQueue scraperQueue = queueClient.GetQueueReference("updatequeue");

            scraperQueue.CreateIfNotExists();

            return scraperQueue;
        }

    }
}
